<<<<<<< HEAD
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This library is part of Cameron Device SDK that allows the use of Cameron devices in your own applications
//
// Copyright 2017 (c) Lenovo.  All rights reserved.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _CAMERONDEVICELIB_H
#define _CAMERONDEVICELIB_H

#include <stdint.h>

#ifdef WIN32
#ifdef CAMERONDEVICELIB_DLL
#define API_FUNCTION(type)	__declspec(dllexport) type __cdecl
#else
#define API_FUNCTION(type)	__declspec(dllimport) type __cdecl
#endif
#define CALLBACK    			__stdcall
#else
#define API_FUNCTION(type)	 	__attribute__((visibility("default"))) type
#define CALLBACK
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CAMERONDEVICELIB C API

extern "C" {

	// Cameron instance
	typedef void *CameronInstance;

	// Cameron resolution info
	struct CameronResolutionInfo
	{
		int   width;                    // Cameron frame width
		int   height;                   // Cameron frame height
		float fps;                      // Cameron frame rate
	};

	struct CameronDisplayCalibration
	{
		unsigned char paddingLeftDisplay[2];					//This is a calibration parameter captured from the calibration jig.
		unsigned char paddingRightDisplay[2];			  //This is a calibration parameter captured from the calibration jig.
	};

	// Cameron IMU data sample
	struct CameronIMUSample
	{
		uint32_t timeStamp;             // Cameron IMU time stamp in 100us increments
		float    accelData[3];             // Cameron accelerometer data (x,y,z) in g units
		float    gyroData[3];      				// Cameron gyroscope data (x,y,z) id degrees/s
		float    magData[3];               // Cameron magnetometer data
	};

#define CAMERON_MAX_IMU_SAMPLES     100

	// Cameron Frame
	struct CameronFrame
	{
		uint32_t  width;															// Cameron frame width
		uint32_t  height;														// Cameron frame height
		uint32_t  timeStamp;													// Cameron frame time stamp in 100us increments
		uint32_t  lightSensor;												// Light sensor reading between 0 and 1000
		uint32_t  proximitySensor;										// Proximity sensor reading, in mm between 0 and 1000
		uint8_t  *cameraData;												// Cameron frame data
		uint32_t  IMUSamplesCount;										// Number of IMU data samples in this frame
		CameronIMUSample IMUData[CAMERON_MAX_IMU_SAMPLES];	// Cameron IMU data samples
	};

	struct CameronIMU_Calibration
	{
		uint32_t meanTimeDelta; //ms

		float gravityDirection[3];// unit vector
		float magnetometerDirection[3];// unit vector
		float accelerometerVariance[3];// g-units^2
		float accelerometerDrift[3];// g-units/s
		float gyroVariance[3];// (rad/s)^2
		float gyroDrift[3];// rad/s
		float magnetometerVariance[3];// units^2
		float magnetometerDrift[3];// units^2
	};
	
	
	//Add
	struct CameronInfo
	{
		/*Device Name*/
		unsigned char *device_name;
		/*Device Lib Version*/
		unsigned char *device_version;
		/* Serial Number */
		unsigned char *serial_number;
		/* Manufacturer String */
		unsigned char *manufacturer_string;
		/* Product string */
		unsigned char *product_string;
		/*FirmwareVersion*/
		unsigned char *firmware_version;
		/*FirmwareBuild*/
		unsigned char *firmware_build;
		/*Led status (ms)*/
		unsigned short led_on_time=0;
		unsigned short led_off_time=0;
		/*Display Status*/
		bool display_left, display_right;
		/*Display Brightness*/
		unsigned char display_left_brightness_r;
		unsigned char display_left_brightness_g;
		unsigned char display_left_brightness_b;
		unsigned char display_right_brightness_r;
		unsigned char display_right_brightness_g;
		unsigned char display_right_brightness_b;
		/*Display Padding*/
		CameronDisplayCalibration display_padding;
	};

#pragma pack(push, 1)
	struct Cameron_INTR
	{
		uint32_t width;
		uint32_t height;

		struct INTR
		{
			float k1, k2, k3;          // Camera radial distortion coefficients
			float k4, k5, k6;          // Camera radial distortion coefficients
			float p1, p2;              // Camera tangential distortion coefficients
			float fx, fy;              // Camera focal lengths in pixel units
			float cx, cy;              // Camera principal point
		};

		INTR camera;
	};

	struct Cameron_EXTR
	{
		float rotation[3];
		float translation[3];
	};
#pragma pack(pop)

	//0- Get CameronLib version string
	API_FUNCTION(char*) GetCameronDeviceLibVersion();

	//0- Cameron device initialization
	API_FUNCTION(bool) OpenCameron(CameronInstance *&cameron);
	API_FUNCTION(bool) CloseCameron(CameronInstance cameron);

	// Cameron frame callback function
	// NOTE: This function is called in the context of the Cameron capture thread.
	//		 To prevent any dropped frames, this function must return as soon as possible.
	typedef void (CALLBACK *CameronFrameCallback)(const CameronFrame * pFrameData, void *pUserData);

	//0- Cameron device capture control
	API_FUNCTION(bool) StartCameron(CameronInstance cameron, CameronFrameCallback frameCallback, void *pUserData);
	API_FUNCTION(bool) StopCameron(CameronInstance cameron);

	//FS- Cameron firmware update
	API_FUNCTION(bool) UpdateFirmware(char * filename);

	// Get Cameron parameters
	//9- Returns Cameron device name in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) GetCameronDeviceName(CameronInstance cameron, char *val, int len);
	//8- Returns Cameron serial number in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) GetCameronSerialNumber(CameronInstance cameron, char *val, int len);
	//7- Returns Cameron firmware version in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) GetCameronFirmwareVersion(CameronInstance cameron, char *val, int len);
	//6- Returns Cameron firmware build information in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) GetCameronFirmwareBuild(CameronInstance cameron, char *val, int len);

	//FS-  Returns Cameron frame width and height
	API_FUNCTION(bool) GetCameronResolutionInfo(CameronInstance cameron, CameronResolutionInfo *info);
	//FS-  Returns Cameron exposure value in percentage [0,100]
	API_FUNCTION(bool) GetCameronExposure(CameronInstance cameron, float *val);
	//FS-  Returns Cameron exposure value in milliseconds
	API_FUNCTION(bool) GetCameronExposureMS(CameronInstance cameron, float *val);
	//FS-  Returns Cameron auto exposure value
	API_FUNCTION(bool) GetCameronAutoExposure(CameronInstance cameron, bool *val);
	//FS-  Returns Cameron gain value in percentage [0,100]
	API_FUNCTION(bool) GetCameronGain(CameronInstance cameron, float *val);
	//FS-  Returns Cameron horizontal image flip value
	API_FUNCTION(bool) GetCameronHFlip(CameronInstance cameron, bool *val);
	//FS-  Returns Cameron vertical image flip value
	API_FUNCTION(bool) GetCameronVFlip(CameronInstance cameron, bool *val);
	//FS-  Returns Cameron calibration present status value
	API_FUNCTION(bool) GetCameronCalibrationPresent(CameronInstance cameron, bool *val);
	//FS-  Returns Cameron field of view for currently selected resolution. User must allocate 2 float values: (HFOV, VFOV)
	API_FUNCTION(bool) GetCameronFOV(CameronInstance cameron, float *val);
	//FS-  Returns Cameron rectified field of view for currently selected resolution. User must allocate 2 float values: (HFOV, VFOV)
	API_FUNCTION(bool) GetCameronRectifiedFOV(CameronInstance cameron, float *val);
	//FS-  Returns Cameron image undistort value
	API_FUNCTION(bool) GetCameronUndistort(CameronInstance cameron, bool *val);
	//FS-  Returns Cameron camera intrinsics parameters, see Cameron_INTR structure
	API_FUNCTION(bool) GetCameronIntrinsics(CameronInstance cameron, Cameron_INTR *val);
	//FS-  Returns Cameron camera extrinsics parameters, see Cameron_EXTR structure
	API_FUNCTION(bool) GetCameronExtrinsics(CameronInstance cameron, Cameron_EXTR *val);

	//4- Returns the current configuration of the status LED. User must allocate two values: time on, time off (ms)
	API_FUNCTION(bool) GetCameronStatusLED(CameronInstance cameron, unsigned short *val);

	//2- Returns whether the displays are on or off. User must allocate 2 bool values: left and right
	API_FUNCTION(bool) GetCameronDisplayStatus(CameronInstance cameron, bool *val);
	//3- Returns the display brigtness. User must allocate 2 uint16_t values: left and right
	API_FUNCTION(bool) GetCameronDisplayBrightness(CameronInstance cameron, unsigned char*val);
	//1- Return the display calibration parameters
	API_FUNCTION(bool) GetCameronDisplayPadding(CameronInstance cameron, CameronDisplayCalibration *val);

	//FS- Returns the IMU calibration paramenters
	API_FUNCTION(bool) GetCameronIMUCalibration(CameronInstance cameron, CameronIMU_Calibration *val);

	// Set Cameron parameters
	// Set current resolution for Cameron.
	//FS- The CameronResolutionInfo is obtained by calling EnumerateCameronResolutions with desired image size, binning and frame rate.
	API_FUNCTION(bool) SetCameronResolutionInfo(CameronInstance cameron, CameronResolutionInfo resInfo);
	//FS- Sets Cameron exposure value in percentage [0,100]
	API_FUNCTION(bool) SetCameronExposure(CameronInstance cameron, float val);
	//FS- Sets Cameron exposure value in milliseconds
	API_FUNCTION(bool) SetCameronExposureMS(CameronInstance cameron, float val);
	//FS- Sets Cameron auto exposure value, default: false. The target exposure value is set using SetCameronExposure.
	API_FUNCTION(bool) SetCameronAutoExposure(CameronInstance cameron, bool val);
	//FS- Sets Cameron gain value in percentage [0,100], default: 0
	API_FUNCTION(bool) SetCameronGain(CameronInstance cameron, float val);
	//FS- Sets Cameron horizontal image flip value, default: false
	API_FUNCTION(bool) SetCameronHFlip(CameronInstance cameron, bool val);
	//FS- Sets Cameron vertical image flip value, default: false
	API_FUNCTION(bool) SetCameronVFlip(CameronInstance cameron, bool val);
	//FS- Sets Cameron image undistort value, default: false
	API_FUNCTION(bool) SetCameronUndistort(CameronInstance cameron, bool val);

	//5- Sets Cameron device name in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) SetCameronDeviceName(CameronInstance cameron, const char *val);
	//FS- Sets Cameron serial number in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) SetCameronSerialNumber(CameronInstance cameron, const char *val);
	//FS- Sets Cameron field of view for currently selected resolution. User must allocate 2 float values: (HFOV, VFOV)
	API_FUNCTION(bool) SetCameronFOV(CameronInstance cameron, float *val);
	//FS- Sets Cameron rectified field of view for currently selected resolution. User must allocate 2 float values: (HFOV, VFOV)
	API_FUNCTION(bool) SetCameronRectifiedFOV(CameronInstance cameron, float *val);
	//FS- Sets Cameron camera intrinsics parameters, see Cameron_INTR structure
	API_FUNCTION(bool) SetCameronIntrinsics(CameronInstance cameron, Cameron_INTR val);
	//FS- Sets Cameron camera extrinsics parameters, see Cameron_EXTR structure
	API_FUNCTION(bool) SetCameronExtrinsics(CameronInstance cameron, Cameron_EXTR val);

	//4- Sets Cameron Status LED, User must allocate two values: time on, time off (ms)
	API_FUNCTION(bool) SetCameronStatusLED(CameronInstance cameron, unsigned short *val);

	//2- Sets whether the displays are on or off. User must provide 2 bool values: left and right
	API_FUNCTION(bool) SetCameronDisplayStatus(CameronInstance cameron, bool *val);
	//3- Sets the display brigtness. User must provide 2 uint32_t values: left and right
	API_FUNCTION(bool) SetCameronDisplayBrightness(CameronInstance cameron, unsigned char *val);
	//1- Sets the display calibration parameters
	API_FUNCTION(bool) SetCameronDisplayPadding(CameronInstance cameron, CameronDisplayCalibration val);

	//FS- Sets the IMU calibration paramenters
	API_FUNCTION(bool) SetCameronIMUCalibration(CameronInstance cameron, CameronIMU_Calibration val);

}




#endif // _CAMERONDEVICELIB_H
=======
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This library is part of Cameron Device SDK that allows the use of Cameron devices in your own applications
//
// Copyright 2017 (c) Lenovo.  All rights reserved.
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _CAMERONDEVICELIB_H
#define _CAMERONDEVICELIB_H

#include <stdint.h>

#ifdef WIN32
#ifdef CAMERONDEVICELIB_DLL
#define API_FUNCTION(type)	__declspec(dllexport) type __cdecl
#else
#define API_FUNCTION(type)	__declspec(dllimport) type __cdecl
#endif
#define CALLBACK    			__stdcall
#else
#define API_FUNCTION(type)	 	__attribute__((visibility("default"))) type
#define CALLBACK
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CAMERONDEVICELIB C API

extern "C" {

	// Cameron instance
	typedef void *CameronInstance;

	// Cameron resolution info
	struct CameronResolutionInfo
	{
		int   width;                    // Cameron frame width
		int   height;                   // Cameron frame height
		float fps;                      // Cameron frame rate
	};

	struct CameronDisplayCalibration
	{
		unsigned char paddingLeftDisplay[2];					//This is a calibration parameter captured from the calibration jig.
		unsigned char paddingRightDisplay[2];			  //This is a calibration parameter captured from the calibration jig.
	};

	// Cameron IMU data sample
	struct CameronIMUSample
	{
		uint32_t timeStamp;             // Cameron IMU time stamp in 100us increments
		float    accelData[3];             // Cameron accelerometer data (x,y,z) in g units
		float    gyroData[3];      				// Cameron gyroscope data (x,y,z) id degrees/s
		float    magData[3];               // Cameron magnetometer data
	};

#define CAMERON_MAX_IMU_SAMPLES     100

	// Cameron Frame
	struct CameronFrame
	{
		uint32_t  width;															// Cameron frame width
		uint32_t  height;														// Cameron frame height
		uint32_t  timeStamp;													// Cameron frame time stamp in 100us increments
		uint32_t  lightSensor;												// Light sensor reading between 0 and 1000
		uint32_t  proximitySensor;										// Proximity sensor reading, in mm between 0 and 1000
		uint8_t  *cameraData;												// Cameron frame data
		uint32_t  IMUSamplesCount;										// Number of IMU data samples in this frame
		CameronIMUSample IMUData[CAMERON_MAX_IMU_SAMPLES];	// Cameron IMU data samples
	};

	struct CameronIMU_Calibration
	{
		uint32_t meanTimeDelta; //ms

		float gravityDirection[3];// unit vector
		float magnetometerDirection[3];// unit vector
		float accelerometerVariance[3];// g-units^2
		float accelerometerDrift[3];// g-units/s
		float gyroVariance[3];// (rad/s)^2
		float gyroDrift[3];// rad/s
		float magnetometerVariance[3];// units^2
		float magnetometerDrift[3];// units^2
	};
	
	
	//Add
	struct CameronInfo
	{
		/*Device Name*/
		unsigned char *device_name;
		/*Device Lib Version*/
		unsigned char *device_version;
		/* Serial Number */
		unsigned char *serial_number;
		/* Manufacturer String */
		unsigned char *manufacturer_string;
		/* Product string */
		unsigned char *product_string;
		/*FirmwareVersion*/
		unsigned char *firmware_version;
		/*FirmwareBuild*/
		unsigned char *firmware_build;
		/*Led status (ms)*/
		unsigned short led_on_time=0;
		unsigned short led_off_time=0;
		/*Display Status*/
		bool display_left, display_right;
		/*Display Brightness*/
		unsigned char display_left_brightness_r;
		unsigned char display_left_brightness_g;
		unsigned char display_left_brightness_b;
		unsigned char display_right_brightness_r;
		unsigned char display_right_brightness_g;
		unsigned char display_right_brightness_b;
		/*Display Padding*/
		CameronDisplayCalibration display_padding;
	};

#pragma pack(push, 1)
	struct Cameron_INTR
	{
		uint32_t width;
		uint32_t height;

		struct INTR
		{
			float k1, k2, k3;          // Camera radial distortion coefficients
			float k4, k5, k6;          // Camera radial distortion coefficients
			float p1, p2;              // Camera tangential distortion coefficients
			float fx, fy;              // Camera focal lengths in pixel units
			float cx, cy;              // Camera principal point
		};

		INTR camera;
	};

	struct Cameron_EXTR
	{
		float rotation[3];
		float translation[3];
	};
#pragma pack(pop)

	//0- Get CameronLib version string
	API_FUNCTION(char*) GetCameronDeviceLibVersion();

	//0- Cameron device initialization
	API_FUNCTION(bool) OpenCameron(CameronInstance *&cameron);
	API_FUNCTION(bool) CloseCameron(CameronInstance cameron);

	// Cameron frame callback function
	// NOTE: This function is called in the context of the Cameron capture thread.
	//		 To prevent any dropped frames, this function must return as soon as possible.
	typedef void (CALLBACK *CameronFrameCallback)(const CameronFrame * pFrameData, void *pUserData);

	//0- Cameron device capture control
	API_FUNCTION(bool) StartCameron(CameronInstance cameron, CameronFrameCallback frameCallback, void *pUserData);
	API_FUNCTION(bool) StopCameron(CameronInstance cameron);

	//FS- Cameron firmware update
	API_FUNCTION(bool) UpdateFirmware(char * filename);

	// Get Cameron parameters
	//9- Returns Cameron device name in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) GetCameronDeviceName(CameronInstance cameron, char *val, int len);
	//8- Returns Cameron serial number in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) GetCameronSerialNumber(CameronInstance cameron, char *val, int len);
	//7- Returns Cameron firmware version in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) GetCameronFirmwareVersion(CameronInstance cameron, char *val, int len);
	//6- Returns Cameron firmware build information in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) GetCameronFirmwareBuild(CameronInstance cameron, char *val, int len);

	//FS-  Returns Cameron frame width and height
	API_FUNCTION(bool) GetCameronResolutionInfo(CameronInstance cameron, CameronResolutionInfo *info);
	//FS-  Returns Cameron exposure value in percentage [0,100]
	API_FUNCTION(bool) GetCameronExposure(CameronInstance cameron, float *val);
	//FS-  Returns Cameron exposure value in milliseconds
	API_FUNCTION(bool) GetCameronExposureMS(CameronInstance cameron, float *val);
	//FS-  Returns Cameron auto exposure value
	API_FUNCTION(bool) GetCameronAutoExposure(CameronInstance cameron, bool *val);
	//FS-  Returns Cameron gain value in percentage [0,100]
	API_FUNCTION(bool) GetCameronGain(CameronInstance cameron, float *val);
	//FS-  Returns Cameron horizontal image flip value
	API_FUNCTION(bool) GetCameronHFlip(CameronInstance cameron, bool *val);
	//FS-  Returns Cameron vertical image flip value
	API_FUNCTION(bool) GetCameronVFlip(CameronInstance cameron, bool *val);
	//FS-  Returns Cameron calibration present status value
	API_FUNCTION(bool) GetCameronCalibrationPresent(CameronInstance cameron, bool *val);
	//FS-  Returns Cameron field of view for currently selected resolution. User must allocate 2 float values: (HFOV, VFOV)
	API_FUNCTION(bool) GetCameronFOV(CameronInstance cameron, float *val);
	//FS-  Returns Cameron rectified field of view for currently selected resolution. User must allocate 2 float values: (HFOV, VFOV)
	API_FUNCTION(bool) GetCameronRectifiedFOV(CameronInstance cameron, float *val);
	//FS-  Returns Cameron image undistort value
	API_FUNCTION(bool) GetCameronUndistort(CameronInstance cameron, bool *val);
	//FS-  Returns Cameron camera intrinsics parameters, see Cameron_INTR structure
	API_FUNCTION(bool) GetCameronIntrinsics(CameronInstance cameron, Cameron_INTR *val);
	//FS-  Returns Cameron camera extrinsics parameters, see Cameron_EXTR structure
	API_FUNCTION(bool) GetCameronExtrinsics(CameronInstance cameron, Cameron_EXTR *val);

	//4- Returns the current configuration of the status LED. User must allocate two values: time on, time off (ms)
	API_FUNCTION(bool) GetCameronStatusLED(CameronInstance cameron, unsigned short *val);

	//2- Returns whether the displays are on or off. User must allocate 2 bool values: left and right
	API_FUNCTION(bool) GetCameronDisplayStatus(CameronInstance cameron, bool *val);
	//3- Returns the display brigtness. User must allocate 2 uint16_t values: left and right
	API_FUNCTION(bool) GetCameronDisplayBrightness(CameronInstance cameron, unsigned char*val);
	//1- Return the display calibration parameters
	API_FUNCTION(bool) GetCameronDisplayPadding(CameronInstance cameron, CameronDisplayCalibration *val);

	//FS- Returns the IMU calibration paramenters
	API_FUNCTION(bool) GetCameronIMUCalibration(CameronInstance cameron, CameronIMU_Calibration *val);

	// Set Cameron parameters
	// Set current resolution for Cameron.
	//FS- The CameronResolutionInfo is obtained by calling EnumerateCameronResolutions with desired image size, binning and frame rate.
	API_FUNCTION(bool) SetCameronResolutionInfo(CameronInstance cameron, CameronResolutionInfo resInfo);
	//FS- Sets Cameron exposure value in percentage [0,100]
	API_FUNCTION(bool) SetCameronExposure(CameronInstance cameron, float val);
	//FS- Sets Cameron exposure value in milliseconds
	API_FUNCTION(bool) SetCameronExposureMS(CameronInstance cameron, float val);
	//FS- Sets Cameron auto exposure value, default: false. The target exposure value is set using SetCameronExposure.
	API_FUNCTION(bool) SetCameronAutoExposure(CameronInstance cameron, bool val);
	//FS- Sets Cameron gain value in percentage [0,100], default: 0
	API_FUNCTION(bool) SetCameronGain(CameronInstance cameron, float val);
	//FS- Sets Cameron horizontal image flip value, default: false
	API_FUNCTION(bool) SetCameronHFlip(CameronInstance cameron, bool val);
	//FS- Sets Cameron vertical image flip value, default: false
	API_FUNCTION(bool) SetCameronVFlip(CameronInstance cameron, bool val);
	//FS- Sets Cameron image undistort value, default: false
	API_FUNCTION(bool) SetCameronUndistort(CameronInstance cameron, bool val);

	//5- Sets Cameron device name in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) SetCameronDeviceName(CameronInstance cameron, const char *val);
	//FS- Sets Cameron serial number in user allocateed string (min size 252 bytes)
	API_FUNCTION(bool) SetCameronSerialNumber(CameronInstance cameron, const char *val);
	//FS- Sets Cameron field of view for currently selected resolution. User must allocate 2 float values: (HFOV, VFOV)
	API_FUNCTION(bool) SetCameronFOV(CameronInstance cameron, float *val);
	//FS- Sets Cameron rectified field of view for currently selected resolution. User must allocate 2 float values: (HFOV, VFOV)
	API_FUNCTION(bool) SetCameronRectifiedFOV(CameronInstance cameron, float *val);
	//FS- Sets Cameron camera intrinsics parameters, see Cameron_INTR structure
	API_FUNCTION(bool) SetCameronIntrinsics(CameronInstance cameron, Cameron_INTR val);
	//FS- Sets Cameron camera extrinsics parameters, see Cameron_EXTR structure
	API_FUNCTION(bool) SetCameronExtrinsics(CameronInstance cameron, Cameron_EXTR val);

	//4- Sets Cameron Status LED, User must allocate two values: time on, time off (ms)
	API_FUNCTION(bool) SetCameronStatusLED(CameronInstance cameron, unsigned short *val);

	//2- Sets whether the displays are on or off. User must provide 2 bool values: left and right
	API_FUNCTION(bool) SetCameronDisplayStatus(CameronInstance cameron, bool *val);
	//3- Sets the display brigtness. User must provide 2 uint32_t values: left and right
	API_FUNCTION(bool) SetCameronDisplayBrightness(CameronInstance cameron, unsigned char *val);
	//1- Sets the display calibration parameters
	API_FUNCTION(bool) SetCameronDisplayPadding(CameronInstance cameron, CameronDisplayCalibration val);

	//FS- Sets the IMU calibration paramenters
	API_FUNCTION(bool) SetCameronIMUCalibration(CameronInstance cameron, CameronIMU_Calibration val);

}




#endif // _CAMERONDEVICELIB_H
>>>>>>> d8b605f20ef7180c384cb2be19a07c16f0a08d5f
