<<<<<<< HEAD
#include <Windows.h>
#include <dshow.h>
#include <atlbase.h>
#include "qedit.h"



#define MYFREEMEDIATYPE(mt) {if ((mt).cbFormat != 0)		\
					{CoTaskMemFree((PVOID)(mt).pbFormat);	\
					(mt).cbFormat = 0;						\
					(mt).pbFormat = NULL;					\
				}											\
				if ((mt).pUnk != NULL)						\
				{											\
				(mt).pUnk->Release();					\
				(mt).pUnk = NULL;						\
				}}				


class DScamera
{
public:
	unsigned char* m_pTemp;
private:
	bool m_bConnected;
	int m_nWidth;
	int m_nHeight;
	int m_nFrameRate;
	bool m_bLock;
	bool m_bChanged;
	long m_nBufferSize;


	int m_nFlipMode;

	CComPtr<IGraphBuilder> m_pGraph;
	CComPtr<IBaseFilter> m_pDeviceFilter;
	CComPtr<IMediaControl> m_pMediaControl;
	CComPtr<IBaseFilter> m_pSampleGrabberFilter;
	CComPtr<ISampleGrabber> m_pSampleGrabber;
	CComPtr<IPin> m_pGrabberInput;
	CComPtr<IPin> m_pGrabberOutput;
	CComPtr<IPin> m_pCameraOutput;
	CComPtr<IMediaEvent> m_pMediaEvent;
	CComPtr<IBaseFilter> m_pNullFilter;
	CComPtr<IPin> m_pNullInputPin;


public:
	DScamera();
	~DScamera();

	//返回翻转信息
	int GetFlipModel();

	//打开摄像头，nCamID指定打开哪个摄像头，取值可以为0,1,2,...
	//bDisplayProperties指示是否自动弹出摄像头属性页
	//nWidth和nHeight设置的摄像头的宽和高，如果摄像头不支持所设定的宽度和高度，则返回false
	bool OpenCamera(int nCamID,
		int nWidth = 640, int nHeight = 480,
		int nFrameRate = 30,
		int nFlip = 0);

	//关闭摄像头，析构函数会自动调用这个函数
	void CloseCamera();

	//返回摄像头的数目
	//可以不用创建CCameraDS实例，采用int c=DScamera::CameraCount();得到结果。
	static int CameraCount();
	
	//根据摄像头的名字返回摄像头编号
	//nCamID: 摄像头编号
	//sName: 用于存放摄像头名字的数组
	//nBufferSize: sName的大小，大小为strlen(sName)
	//可以不用创建CCameraDS实例，采用DScamera::CameraName();得到结果。
	static int CameraIndex(int& nCamID,const char* sName,int nBufferSize);

	//根据摄像头的编号返回摄像头的名字
	//nCamID: 摄像头编号
	//sName: 用于存放摄像头名字的数组
	//nBufferSize: sName的大小
	//可以不用创建CCameraDS实例，采用DScamera::CameraName();得到结果。
	static int CameraName(int nCamID, char* sName, int nBufferSize);

	//根据摄像头的编号返回摄像头的描述符
	//nCamID: 摄像头编号
	//sName: 用于存放摄像头描述符的数组
	//nBufferSize: sName的大小
	//可以不用创建CCameraDS实例，采用DScamera::CameraDescription();得到结果。
	static int CameraDescription(int nCamID, char* sName, int nBufferSize);

	//根据摄像头的编号返回摄像头的设备路径
	//nCamID: 摄像头编号
	//sName: 用于存放摄像头名字的数组
	//nBufferSize: sName的大小
	//可以不用创建CCameraDS实例，采用DScamera::CameraDevicePath();得到结果。
	static int CameraDevicePath(int nCamID, char* sName, int nBufferSize);


	//返回图像宽度
	int GetWidth() { return m_nWidth; }

	//返回图像高度
	int GetHeight() { return m_nHeight; }

	//设置图像分辨率
	bool SetResolution(int nCamID, int nWidth, int nHeight);

	//设置图像亮度
	void SetBrightness(int brightPersent);

	//设置图像对比度
	void SetConstrast(int constrastPersent);

	//设置图像饱和度
	void SetSaturation(int saturationPersent);

	//设置色彩
	void SetHue(int huePersent);

	//设置平滑
	void SetSharpness(int sharpnessPersent);

	//设置Gamma
	void SetGamma(int gammaPersent);

	//设置帧率
	bool SetFrameRate(int nCamID, int nFrameRate);

	//设置翻转
	bool SetFlip(int nCamID,int nFlip);

	//Todo
	 void QueryFrame();
	 //将Frame存储至指定buf
	 //存储成功返回0，失败返回错误码
	 //错误码：-1：buf为空，-2bufsize空间不足
	 int QueryFrame(unsigned char* buf,int bufsize);

private:

	bool BindFilter(int nCamIDX, IBaseFilter **pFilter);

};


=======
#include <Windows.h>
#include <dshow.h>
#include <atlbase.h>
#include "qedit.h"



#define MYFREEMEDIATYPE(mt) {if ((mt).cbFormat != 0)		\
					{CoTaskMemFree((PVOID)(mt).pbFormat);	\
					(mt).cbFormat = 0;						\
					(mt).pbFormat = NULL;					\
				}											\
				if ((mt).pUnk != NULL)						\
				{											\
				(mt).pUnk->Release();					\
				(mt).pUnk = NULL;						\
				}}				


class DScamera
{
public:
	unsigned char* m_pTemp;
private:
	bool m_bConnected;
	int m_nWidth;
	int m_nHeight;
	int m_nFrameRate;
	bool m_bLock;
	bool m_bChanged;
	long m_nBufferSize;


	int m_nFlipMode;

	CComPtr<IGraphBuilder> m_pGraph;
	CComPtr<IBaseFilter> m_pDeviceFilter;
	CComPtr<IMediaControl> m_pMediaControl;
	CComPtr<IBaseFilter> m_pSampleGrabberFilter;
	CComPtr<ISampleGrabber> m_pSampleGrabber;
	CComPtr<IPin> m_pGrabberInput;
	CComPtr<IPin> m_pGrabberOutput;
	CComPtr<IPin> m_pCameraOutput;
	CComPtr<IMediaEvent> m_pMediaEvent;
	CComPtr<IBaseFilter> m_pNullFilter;
	CComPtr<IPin> m_pNullInputPin;


public:
	DScamera();
	~DScamera();

	//返回翻转信息
	int GetFlipModel();

	//打开摄像头，nCamID指定打开哪个摄像头，取值可以为0,1,2,...
	//bDisplayProperties指示是否自动弹出摄像头属性页
	//nWidth和nHeight设置的摄像头的宽和高，如果摄像头不支持所设定的宽度和高度，则返回false
	bool OpenCamera(int nCamID,
		int nWidth = 640, int nHeight = 480,
		int nFrameRate = 30,
		int nFlip = 0);

	//关闭摄像头，析构函数会自动调用这个函数
	void CloseCamera();

	//返回摄像头的数目
	//可以不用创建CCameraDS实例，采用int c=DScamera::CameraCount();得到结果。
	static int CameraCount();
	
	//根据摄像头的名字返回摄像头编号
	//nCamID: 摄像头编号
	//sName: 用于存放摄像头名字的数组
	//nBufferSize: sName的大小，大小为strlen(sName)
	//可以不用创建CCameraDS实例，采用DScamera::CameraName();得到结果。
	static int CameraIndex(int& nCamID,const char* sName,int nBufferSize);

	//根据摄像头的编号返回摄像头的名字
	//nCamID: 摄像头编号
	//sName: 用于存放摄像头名字的数组
	//nBufferSize: sName的大小
	//可以不用创建CCameraDS实例，采用DScamera::CameraName();得到结果。
	static int CameraName(int nCamID, char* sName, int nBufferSize);

	//根据摄像头的编号返回摄像头的描述符
	//nCamID: 摄像头编号
	//sName: 用于存放摄像头描述符的数组
	//nBufferSize: sName的大小
	//可以不用创建CCameraDS实例，采用DScamera::CameraDescription();得到结果。
	static int CameraDescription(int nCamID, char* sName, int nBufferSize);

	//根据摄像头的编号返回摄像头的设备路径
	//nCamID: 摄像头编号
	//sName: 用于存放摄像头名字的数组
	//nBufferSize: sName的大小
	//可以不用创建CCameraDS实例，采用DScamera::CameraDevicePath();得到结果。
	static int CameraDevicePath(int nCamID, char* sName, int nBufferSize);


	//返回图像宽度
	int GetWidth() { return m_nWidth; }

	//返回图像高度
	int GetHeight() { return m_nHeight; }

	//设置图像分辨率
	bool SetResolution(int nCamID, int nWidth, int nHeight);

	//设置图像亮度
	void SetBrightness(int brightPersent);

	//设置图像对比度
	void SetConstrast(int constrastPersent);

	//设置图像饱和度
	void SetSaturation(int saturationPersent);

	//设置色彩
	void SetHue(int huePersent);

	//设置平滑
	void SetSharpness(int sharpnessPersent);

	//设置Gamma
	void SetGamma(int gammaPersent);

	//设置帧率
	bool SetFrameRate(int nCamID, int nFrameRate);

	//设置翻转
	bool SetFlip(int nCamID,int nFlip);

	//Todo
	 void QueryFrame();
	 //将Frame存储至指定buf
	 //存储成功返回0，失败返回错误码
	 //错误码：-1：buf为空，-2bufsize空间不足
	 int QueryFrame(unsigned char* buf,int bufsize);

private:

	bool BindFilter(int nCamIDX, IBaseFilter **pFilter);

};


>>>>>>> d8b605f20ef7180c384cb2be19a07c16f0a08d5f
