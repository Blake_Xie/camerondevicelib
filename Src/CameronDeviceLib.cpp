<<<<<<< HEAD


#include <CameronDeviceLib.h>

#include <iostream>

#include <windows.h>
#include <tchar.h>
#include <strsafe.h>

//include DScamera
#include <UVCcamera\DScam.h>

//include HIDapi
#include <HID\hidapi.h>

#pragma comment(lib, "strmiids")

#pragma region Constance and Macro

#pragma region Send Package

const unsigned char REPORT_ID = 0x01;

//CAMERON_REQ_TYPE------Byte1
#define REQ_SET 0x00
#define REQ_GET 0x01

//CAMERON_REQ_TARGET------Byte2
#define REQ_DEVICE  0x00
#define REQ_DISPLAY  0x01
#define REQ_SENSOR_IMU  0X02
#define REQ_SENSOR_ALS  0X03
#define PEQ_SENSOR_PXM  0x04

//CAMERON_REQ_PURPOSE_DEVICE------Byte3
#define REQ_OPEN  0x00                
#define REQ_CLOSE  0x01                 
#define REQ_START  0x02                 
#define REQ_STOP  0x03            
#define REQ_PARAMS_DEVICE_NAME 0x04
#define REQ_PARAMS_SERIAL_NUM 0x05
#define REQ_PARAMS_FW_VERSION 0x06
#define REQ_PARAMS_FW_BUILD 0x07
#define REQ_PARAMS_LED 0x08

//CAMERON_REQ_PURPOSE_DISPLAY
#define REQ_ON 0x00
#define REQ_OFF 0x01
#define REQ_STATUS  0x02      
#define REQ_PADDING 0x03  
#define REQ_BRIGHTNESS 0x04

//Param1-----------Byte4
#define LED_ON_TIME  0x01 //0x00~0xFF
#define DISPLAY_LEFT 0X00
#define DISPLAY_RIGHT 0X01
//Param2-----------Byte5
#define LED_OFF_TIME  0x01//0x00~0xFF
//Param3-----------Byte6
//Param4-----------Byte7
#pragma endregion

#pragma region Receive Package

//ifError---------first byte
#define RES_NO_ERROR 0x00
#define RES_ERROR 0x01

//Receiver-----second byte
#define RES_DEVICE 0x00
#define RES_DISPLAY 0x01
#define RES_SENSOR_IMU 0x02
#define RES_SENSOR_ALS 0x03
#define RES_SENSOR_PXM 0x04

//Purpose-----third byte
//the same as send package

//Param1-----forth byte
#define LEFT_ON 0x00
#define LEFT_OFF 0x01

#define LEFT_DISPLAY 0x00
#define RIGHT_DISPLAY 0x01
//Param2-----fifth byte
#define RIGHT_ON 0x00
#define RIGHT_OFF 0x01
//Param3-----sixth byte
//Param4-----seventh byte

#pragma endregion

#define CAMERAONVID 0x0483
#define CAMERAONPID 0x5750
#define MAX_STR 255
#define SENT_LENGTH 64
#define IMU_Mag_RANGE_XY 1300
#define IMU_Mag_RANGE_Z 2500
#define IMU_GYRO_RANGE 500
#define IMU_GRAVITY 9.81
#define IMU_ACCL_RANGE (4*IMU_GRAVITY)



#pragma endregion

#pragma region Type Defind

typedef DScamera* DScamea_instance;

typedef hid_device* HID_instance;

typedef struct MyData {
	CameronInstance instance;
	CameronFrameCallback callback;
	void *pUserData;
} MYDATA, *PMYDATA;

#pragma endregion

#pragma region  Global variable

//DS camera
const char* DScameraName = "Logitech HD Pro Webcam C920";

//Instance
CameronInstance instance;
CameronFrameCallback callback = NULL;
DScamea_instance dscam_instance = NULL;
HID_instance hid_instance = NULL;
CameronFrame * frame = NULL;
CameronIMUSample *imuframe = NULL;
CameronInfo *cameron_info = NULL;
int dscamIndex = 0;

bool           m_bRunning = false;

// Sample custom data structure for threads to use.
// This is passed by void pointer so it can be any data type
// that can be passed using a single void pointer (LPVOID).

PMYDATA pData;
DWORD   dwThreadId;
HANDLE  hThread;
DWORD   dwHIDThreadId;
HANDLE  hHIDThread;

/* END - These elements are temporary stuff to test the Dashboard */


char* version = "0.1"; //this string is stored in this CPP
#pragma endregion

/* These elements are temporary stuff to test the Dashboard */

DWORD WINAPI CameronFrameFunction(LPVOID lpParam);
DWORD WINAPI HIDFrameFunction(LPVOID lpParam);
void ErrorHandler(LPTSTR lpszFunction);


//allocate and free the CameronInfo
bool allocCameronInfo(CameronInfo *&info);
bool freeCameronInfo(CameronInfo *&info);

//Send HID package
bool SendPackage(int lenth, byte firstbyte,
	byte secondbyte,
	byte thirdbyte,
	byte forthbyte=0x00,
	byte fifthbyte=0x00,
	byte sixthbyte=0x00,
	byte seventhbyte=0x00,
	byte* content=NULL,
	int contentlenth=0);

//Encode HID echo package
bool EncodePackage(unsigned char *&package, int lenth,
	byte CAMERON_REQ_TYPE,
	byte CAMERON_REQ_TARGET,
	byte CAMERON_REQ_PURPOSE_DEVICE,
	byte LED_Param1,
	byte LED_Param2,
	byte LED_Param3,
	byte LED_Param4,
	byte* content,
	int content_lenth);
//Decode HID echo package
bool DecodePackage(unsigned char* package, int lenth);
void AnalysisIMU(unsigned char* src_data, CameronIMUSample* des_data);
void analysisMAG(uint8_t* src, int16_t* det);
void analysisGYR(uint8_t* src, int16_t* det);
void analysisACC(uint8_t* src, int16_t* det);
//utility

//width char to char
bool wchar2char(wchar_t* &pwchar, char* pchar);
//copy memory to dest, and move the dest point to the end
bool CopyAndMove(unsigned char*& pDst, const unsigned char* pSrc, int lenth);

API_FUNCTION(char*) GetCameronDeviceLibVersion()
{
	return version;
}

API_FUNCTION(bool) OpenCameron(CameronInstance *&cameron)
{
	//Creates the local instance 
	cameron = new CameronInstance();
	instance = cameron;
	dscam_instance = new DScamera();
	frame = new CameronFrame();
	imuframe = new CameronIMUSample();
	if (hid_init() < 0)
	{
		ErrorHandler(TEXT("Hid Device initial error"));
		return false;
	}

	//OpenHID Device
	{
		hid_instance = hid_open(CAMERAONVID, CAMERAONPID, NULL);
		if (!hid_instance)
		{
			ErrorHandler(TEXT("HID device open fail"));
			return false;
		}
		// Set the hid_read() function to be non-blocking.
		hid_set_nonblocking(hid_instance, 1);
		
		hHIDThread = CreateThread(
			NULL,                   // default security attributes
			0,                      // use default stack size  
			HIDFrameFunction,       // thread function name
			pData,								// argument to thread function 
			0,                      // use default creation flags 
			&dwHIDThreadId);						// returns the thread identifier 

		if (hHIDThread == NULL)
			ErrorHandler(TEXT("Create HID Thread"));
	
	CloseHandle(hHIDThread);
	}

	//Get HID info
	{
		if (cameron_info == NULL)
			allocCameronInfo(cameron_info);

		GetCameronSerialNumber(instance, (char *)(cameron_info->serial_number), MAX_STR);
	}
	//open the connection to the MCU via USB
	{
	}

	return true;
}

API_FUNCTION(bool) CloseCameron(CameronInstance cameron)
{

	hid_close(hid_instance);

	hid_exit();

	hid_instance = NULL;

	if (dscam_instance != NULL)
		delete dscam_instance;

	dscam_instance = NULL;

	if (frame != NULL)
		delete frame;

	if (imuframe != NULL)
		delete imuframe;
	imuframe = NULL;

	freeCameronInfo(cameron_info);
	frame = NULL;
	//closes the connection to the MCU via USB
	instance = NULL;

	return true;
}

API_FUNCTION(bool) StartCameron(CameronInstance cameron, CameronFrameCallback frameCallback, void * pUserData)
{


	//tells cameron to start sending camera images, IMU and to turn on the displays
	callback = frameCallback;

	//temporary code for teting the CameronFrameViewer
	{
		std::cout << "Starting Thread" << std::endl;
		std::cout << "CameronInstance: " << instance << ", CameronCallback: " << callback << std::endl;

		pData = (PMYDATA)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(MYDATA));
		pData->callback = callback;
		pData->instance = instance;
		pData->pUserData = pUserData;

		//open Camera with name
		if (dscam_instance!=NULL
			&&DScamera::CameraCount() > 0 
			&& DScamera::CameraIndex(dscamIndex, DScameraName, strlen(DScameraName)))
		{
			dscam_instance->OpenCamera(dscamIndex, 1280, 720, 30, 0);
		}

		m_bRunning = true;

		hThread = CreateThread(
			NULL,                   // default security attributes
			0,                      // use default stack size  
			CameronFrameFunction,       // thread function name
			pData,								// argument to thread function 
			0,                      // use default creation flags 
			&dwThreadId);						// returns the thread identifier 

		if (hThread == NULL)
			ErrorHandler(TEXT("CreateThread"));
	}
	CloseHandle(hThread);
	return true;
}

API_FUNCTION(bool) StopCameron(CameronInstance cameron)
{
	//tells cameron to stop sending all kind of data
	instance = NULL;
	
	//temporary code for teting the CameronFrameViewer
	{
		WaitForSingleObject(hThread, INFINITE);
		CloseHandle(hThread);
	}
	dscam_instance->CloseCamera();
	return true;
}

API_FUNCTION(bool) UpdateFirmware(char * filename)
{
	//load firmware and send it to the device
	std::cout << "Firmware Filename: " << filename << std::endl;
	return true;
}

char * stringStoredInROM = "BRING NAME FROM ROM";
API_FUNCTION(bool) GetCameronDeviceName(CameronInstance cameron, char * val, int len)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DEVICE,
		REQ_PARAMS_DEVICE_NAME);

	if (cameron_info->device_name != NULL) {
		_memccpy(val, cameron_info->device_name, 0, len);
		return true;
	}
	_memccpy(val, "get device error", 0, len);
	return false;
}

API_FUNCTION(bool) GetCameronSerialNumber(CameronInstance cameron, char * val, int len)
{
	if (!hid_instance) {
		ErrorHandler(TEXT("Can't Open HID device"));
		return false;
	}

	wchar_t* wstr = new wchar_t(len/ 2);

	int res = hid_get_serial_number_string(hid_instance,wstr, (len / 2 > MAX_STR / 2) ? MAX_STR / 2 : len / 2);


	if (res < 0)
	{
		ErrorHandler(TEXT("Get Serial Number Error"));
		_memccpy(val, "Get Serial Number Error1", 0, len);
		return false;
	}

	//_memccpy(val, stringStoredInROM, 0, len);


	return wchar2char(wstr,val);

}

API_FUNCTION(bool) GetCameronFirmwareVersion(CameronInstance cameron, char * val, int len)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DEVICE,
		REQ_PARAMS_FW_VERSION);

	if (cameron_info->firmware_version != NULL) {
		_memccpy(val, cameron_info->firmware_version, 0, len);
		return true;
	}
	_memccpy(val, "get FirmwareVersion error", 0, len);
	return false;
}

API_FUNCTION(bool) GetCameronFirmwareBuild(CameronInstance cameron, char * val, int len)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DEVICE,
		REQ_PARAMS_FW_BUILD);

	if (cameron_info->firmware_build != NULL) {
		_memccpy(val, cameron_info->firmware_build, 0, len);
		return true;
	}
	_memccpy(val, "get firmware_build error", 0, len);
	return false;
}

API_FUNCTION(bool) GetCameronResolutionInfo(CameronInstance cameron, CameronResolutionInfo *info)
{
	if (cameron == NULL)
	{
		info->width = 1280;
		info->height = 720;
		info->fps = 30;
	}
	else
	{
		//queries the MCU for available resolutions
	}
	return true;
}

API_FUNCTION(bool) GetCameronExposure(CameronInstance cameron, float * val)
{
	//bring value from MCU
	(*val) = 123.0f;
	return true;
}

API_FUNCTION(bool) GetCameronExposureMS(CameronInstance cameron, float * val)
{
	//bring value from MCU
	(*val) = 432.0f;
	return true;
}

API_FUNCTION(bool) GetCameronAutoExposure(CameronInstance cameron, bool * val)
{
	//bring value from MCU
	(*val) = true;
	return true;
}

API_FUNCTION(bool) GetCameronGain(CameronInstance cameron, float * val)
{
	//bring value from MCU
	(*val) = 333.0f;
	return true;
}

API_FUNCTION(bool) GetCameronHFlip(CameronInstance cameron, bool * val)
{
	//bring value from MCU
	(*val) = true;
	return true;
}

API_FUNCTION(bool) GetCameronVFlip(CameronInstance cameron, bool * val)
{
	//bring value from MCU
	(*val) = true;
	return true;
}

API_FUNCTION(bool) GetCameronCalibrationPresent(CameronInstance cameron, bool * val)
{
	//bring value from MCU
	(*val) = true;
	return true;
}

API_FUNCTION(bool) GetCameronFOV(CameronInstance cameron, float * val)
{
	//bring values from MCU
	val[0] = 111.0f;
	val[1] = 222.0f;
	return true;
}

API_FUNCTION(bool) GetCameronRectifiedFOV(CameronInstance cameron, float * val)
{
	//bring values from MCU
	val[0] = 333.0f;
	val[1] = 444.0f;
	return true;
}

API_FUNCTION(bool) GetCameronUndistort(CameronInstance cameron, bool * val)
{
	//bring value from MCU
	(*val) = true;
	return true;
}

API_FUNCTION(bool) GetCameronIntrinsics(CameronInstance cameron, Cameron_INTR * val)
{
	//bring values from MCU
	val->width = 1280;
	val->height = 720;

	val->camera.cx = 1;
	val->camera.cy = 2;
	val->camera.fx = 3;
	val->camera.fy = 4;
	val->camera.p1 = 5;
	val->camera.p2 = 6;
	val->camera.k1 = 7;
	val->camera.k2 = 8;
	val->camera.k3 = 9;
	val->camera.k4 = 10;
	val->camera.k5 = 11;
	val->camera.k6 = 12;

	return true;
}

API_FUNCTION(bool) GetCameronExtrinsics(CameronInstance cameron, Cameron_EXTR * val)
{
	//bring values from MCU
	val->rotation[0] = 6.0f;
	val->rotation[1] = 5.0f;
	val->rotation[2] = 4.0f;
	val->translation[0] = 3.0f;
	val->translation[1] = 2.0f;
	val->translation[2] = 1.0f;

	return true;
}

API_FUNCTION(bool) GetCameronStatusLED(CameronInstance cameron, unsigned short * val)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DEVICE,
		REQ_PARAMS_LED);

	//bring values from MCU
	val[0] = cameron_info->led_on_time;
	val[1] = cameron_info->led_off_time;

	return true;
}

API_FUNCTION(bool) GetCameronDisplayStatus(CameronInstance cameron, bool *val)
{

	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DISPLAY,
		REQ_STATUS);


	//bring values from MCU
	val[0] = cameron_info->display_left;
	val[1] = cameron_info->display_right;

	return true;
}

API_FUNCTION(bool) GetCameronDisplayBrightness(CameronInstance cameron, unsigned char * val)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DISPLAY,
		REQ_BRIGHTNESS);


	//bring values from MCU
	val[0] = cameron_info->display_left_brightness_r;
	val[1] = cameron_info->display_left_brightness_g;
	val[2] = cameron_info->display_left_brightness_b;

	val[3] = cameron_info->display_right_brightness_r;
	val[4] = cameron_info->display_right_brightness_g;
	val[5] = cameron_info->display_right_brightness_b;

	return true;
}

API_FUNCTION(bool) GetCameronDisplayPadding(CameronInstance cameron, CameronDisplayCalibration * val)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DISPLAY,
		REQ_PADDING);



	//bring values from MCU
	val->paddingLeftDisplay[0] = cameron_info->display_padding.paddingLeftDisplay[0]; //left
	val->paddingLeftDisplay[1] = cameron_info->display_padding.paddingLeftDisplay[1]; //top
	val->paddingRightDisplay[0] = cameron_info->display_padding.paddingRightDisplay[0]; //left
	val->paddingRightDisplay[1] = cameron_info->display_padding.paddingRightDisplay[1]; //top

	return true;
}

API_FUNCTION(bool) GetCameronIMUCalibration(CameronInstance cameron, CameronIMU_Calibration * val)
{
	//bring values from MCU
	val->meanTimeDelta = 77788; //ms

	val->gravityDirection[0] = 999.0f;// unit vector
	val->gravityDirection[1] = 998.0f;// unit vector
	val->gravityDirection[2] = 997.0f;// unit vector

	val->magnetometerDirection[0] = 989.0f;// unit vector
	val->magnetometerDirection[1] = 988.0f;// unit vector
	val->magnetometerDirection[2] = 987.0f;// unit vector

	val->accelerometerVariance[0] = 979.0f;// g-units^2
	val->accelerometerVariance[1] = 978.0f;// g-units^2
	val->accelerometerVariance[2] = 977.0f;// g-units^2

	val->accelerometerDrift[0] = 969.0f;// g-units/s
	val->accelerometerDrift[1] = 968.0f;// g-units/s
	val->accelerometerDrift[2] = 967.0f;// g-units/s

	val->gyroVariance[0] = 959.0f;// (rad/s)^2
	val->gyroVariance[1] = 958.0f;// (rad/s)^2
	val->gyroVariance[2] = 957.0f;// (rad/s)^2

	val->gyroDrift[0] = 949.0f;// rad/s
	val->gyroDrift[1] = 948.0f;// rad/s
	val->gyroDrift[2] = 947.0f;// rad/s

	val->magnetometerVariance[0] = 939.0f;// units^2
	val->magnetometerVariance[1] = 938.0f;// units^2
	val->magnetometerVariance[2] = 937.0f;// units^2

	val->magnetometerDrift[0] = 929.0f;// units^2
	val->magnetometerDrift[1] = 928.0f;// units^2
	val->magnetometerDrift[2] = 927.0f;// units^2

	return true;
}

API_FUNCTION(bool) SetCameronResolutionInfo(CameronInstance cameron, CameronResolutionInfo resInfo)
{
	//send values to MCU
	std::cout << "Width: " << resInfo.width << ", Height: " << resInfo.height << ", FPS: " << resInfo.fps << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronExposure(CameronInstance cameron, float val)
{
	//send values to MCU
	std::cout << "Exposure: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronExposureMS(CameronInstance cameron, float val)
{
	//send values to MCU
	std::cout << "Exposure MS: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronAutoExposure(CameronInstance cameron, bool val)
{
	//send values to MCU
	std::cout << "Auto Exposure: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronGain(CameronInstance cameron, float val)
{
	//send values to MCU
	std::cout << "Gain: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronHFlip(CameronInstance cameron, bool val)
{
	//send values to MCU
	std::cout << "Horizontal Flip: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronVFlip(CameronInstance cameron, bool val)
{
	//send values to MCU
	std::cout << "Vertical Flip: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronUndistort(CameronInstance cameron, bool val)
{
	//send values to MCU
	std::cout << "Undistort: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronDeviceName(CameronInstance cameron, const char * val)
{
	if (strlen(val) > 56)
		return false;

	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DEVICE,
		REQ_PARAMS_DEVICE_NAME,
		(byte)strlen(val),
		0x00,
		0x00,
		0x00,
		(byte*)val,
		strlen(val));

	//TODO

	return true;
}

API_FUNCTION(bool) SetCameronSerialNumber(CameronInstance cameron, const char * val)
{
	if (strlen(val) > 56)
		return false;

	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DEVICE,
		REQ_PARAMS_SERIAL_NUM,
		(byte)strlen(val),
		0x00,
		0x00,
		0x00,
		(byte*)val,
		strlen(val));


	//TODO

	return true;
}

API_FUNCTION(bool) SetCameronFOV(CameronInstance cameron, float * val)
{
	//send values to MCU
	std::cout << "Horizontal FOV: " << val[0] << ", Vertical FOV: " << val[1] << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronRectifiedFOV(CameronInstance cameron, float * val)
{
	//send values to MCU
	std::cout << "Horizontal Rectified FOV: " << val[0] << ", Vertical Rectified FOV: " << val[1] << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronIntrinsics(CameronInstance cameron, Cameron_INTR val)
{
	//send values to MCU
	std::cout << "Intrinsics: " << std::endl;
	std::cout << " - Width: " << val.width << std::endl;
	std::cout << " - Height: " << val.height << std::endl;
	std::cout << " - cx: " << val.camera.cx << std::endl;
	std::cout << " - cy: " << val.camera.cy << std::endl;
	std::cout << " - fx: " << val.camera.fx << std::endl;
	std::cout << " - fy: " << val.camera.fy << std::endl;
	std::cout << " - k1: " << val.camera.k1 << std::endl;
	std::cout << " - k2: " << val.camera.k2 << std::endl;
	std::cout << " - k3: " << val.camera.k3 << std::endl;
	std::cout << " - k4: " << val.camera.k4 << std::endl;
	std::cout << " - k5: " << val.camera.k5 << std::endl;
	std::cout << " - k6: " << val.camera.k6 << std::endl;
	std::cout << " - p1: " << val.camera.p1 << std::endl;
	std::cout << " - p2: " << val.camera.p2 << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronExtrinsics(CameronInstance cameron, Cameron_EXTR val)
{
	//send values to MCU
	std::cout << "Extrinsics: " << std::endl;
	std::cout << " - rotationX: " << val.rotation[0] << std::endl;
	std::cout << " - rotationY: " << val.rotation[1] << std::endl;
	std::cout << " - rotationZ: " << val.rotation[2] << std::endl;
	std::cout << " - translationX: " << val.translation[0] << std::endl;
	std::cout << " - translationY: " << val.translation[1] << std::endl;
	std::cout << " - translationZ: " << val.translation[2] << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronStatusLED(CameronInstance cameron, unsigned short * val)
{
	byte* pval = (byte*)val;
	
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DEVICE,
		REQ_PARAMS_LED,
		*pval,
		*(pval+1),
		*(pval + 2),
		*(pval + 3),
		NULL,
		0);


	//TODO

	return true;
}

API_FUNCTION(bool) SetCameronDisplayStatus(CameronInstance cameron, bool * val)
{
	//todo
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DISPLAY,
		REQ_STATUS,
		*(byte*)val,
		*((byte*)val+1)
		);

	//send values to MCU

	return true;
}

API_FUNCTION(bool) SetCameronDisplayBrightness(CameronInstance cameron, unsigned char * val)
{
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DISPLAY,
		REQ_BRIGHTNESS,
		DISPLAY_LEFT,
		*(byte*)val,
		*((byte*)val + 1),
		*((byte*)val + 2)
	);
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DISPLAY,
		REQ_BRIGHTNESS,
		DISPLAY_RIGHT,
		*(byte*)val,
		*((byte*)val + 1),
		*((byte*)val + 2)
	);

	//send values to MCU
	//std::cout << "Left Display Brightness: " << val[0] << ", Right Display Brightness: " << val[1] << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronDisplayPadding(CameronInstance cameron, CameronDisplayCalibration val)
{
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DISPLAY,
		REQ_PADDING,
		DISPLAY_LEFT,
		val.paddingLeftDisplay[0],
		val.paddingLeftDisplay[1]
	);
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DISPLAY,
		REQ_PADDING,
		DISPLAY_RIGHT,
		val.paddingRightDisplay[0],
		val.paddingRightDisplay[1]
	);


	//send values to MCU
	std::cout << "Display Padding: " << std::endl;
	std::cout << " - Left Display Left: " << val.paddingLeftDisplay[0] << ", Left Display Top: " << val.paddingLeftDisplay[1] << std::endl;
	std::cout << " - Right Display Left: " << val.paddingRightDisplay[0] << ", Right Display Top: " << val.paddingRightDisplay[1] << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronIMUCalibration(CameronInstance cameron, CameronIMU_Calibration val)
{
	//send values to MCU
	std::cout << "IMU Calibration: " << std::endl;
	std::cout << " - meanTimeDelta: " << val.meanTimeDelta << std::endl;

	std::cout << " - gravityDirectionX: " << val.gravityDirection[0] << std::endl;
	std::cout << " - gravityDirectionY: " << val.gravityDirection[1] << std::endl;
	std::cout << " - gravityDirectionZ: " << val.gravityDirection[2] << std::endl;

	std::cout << " - magnetometerDirectionX: " << val.magnetometerDirection[0] << std::endl;
	std::cout << " - magnetometerDirectionY: " << val.magnetometerDirection[1] << std::endl;
	std::cout << " - magnetometerDirectionZ: " << val.magnetometerDirection[2] << std::endl;

	std::cout << " - accelerometerVarianceX: " << val.accelerometerVariance[0] << std::endl;
	std::cout << " - accelerometerVarianceY: " << val.accelerometerVariance[1] << std::endl;
	std::cout << " - accelerometerVarianceZ: " << val.accelerometerVariance[2] << std::endl;

	std::cout << " - accelerometerDriftX: " << val.accelerometerDrift[0] << std::endl;
	std::cout << " - accelerometerDriftY: " << val.accelerometerDrift[1] << std::endl;
	std::cout << " - accelerometerDriftZ: " << val.accelerometerDrift[2] << std::endl;

	std::cout << " - gyroVarianceX: " << val.gyroVariance[1] << std::endl;
	std::cout << " - gyroVarianceY: " << val.gyroVariance[2] << std::endl;
	std::cout << " - gyroVarianceZ: " << val.gyroVariance[0] << std::endl;

	std::cout << " - gyroDriftX: " << val.gyroDrift[0] << std::endl;
	std::cout << " - gyroDriftY: " << val.gyroDrift[1] << std::endl;
	std::cout << " - gyroDriftZ: " << val.gyroDrift[2] << std::endl;

	std::cout << " - magnetometerVarianceX: " << val.magnetometerVariance[0] << std::endl;
	std::cout << " - magnetometerVarianceY: " << val.magnetometerVariance[1] << std::endl;
	std::cout << " - magnetometerVarianceZ: " << val.magnetometerVariance[2] << std::endl;

	std::cout << " - magnetometerDriftX: " << val.magnetometerDrift[0] << std::endl;
	std::cout << " - magnetometerDriftY: " << val.magnetometerDrift[1] << std::endl;
	std::cout << " - magnetometerDriftZ: " << val.magnetometerDrift[2] << std::endl;

	return true;
}

//----------------------- This is to test that a background thread can update the image -----------------------///

DWORD WINAPI CameronFrameFunction(LPVOID lpParam)
{
	HANDLE hStdout;
	PMYDATA pData;

	TCHAR msgBuf[250];
	size_t cchStringSize;
	DWORD dwChars;


	// Make sure there is a console to receive output results. 

	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hStdout == INVALID_HANDLE_VALUE)
		return 1;

	// Cast the parameter to the correct data type.
	// The pointer is known to be valid because 
	// it was checked for NULL before the thread was created.
	pData = (PMYDATA)lpParam;

	// Print the parameter values using thread-safe functions.
	std::cout << "CameronInstance: " << pData->instance << ", CameronCallback: " << pData->callback << std::endl;

	frame->timeStamp = 5678;
	frame->width = 320;
	frame->height = 180;
	frame->lightSensor = 9999;
	frame->proximitySensor = 8888;
	frame->IMUSamplesCount = 1;
	frame->IMUData[0].timeStamp = 5678;
	frame->IMUData[0].accelData[0] = 1.0f;
	frame->IMUData[0].accelData[1] = 1.0f;
	frame->IMUData[0].accelData[2] = 1.0f;
	frame->IMUData[0].gyroData[0] = 2.0f;
	frame->IMUData[0].gyroData[1] = 2.0f;
	frame->IMUData[0].gyroData[2] = 2.0f;
	frame->IMUData[0].magData[0] = 3.0f;
	frame->IMUData[0].magData[1] = 3.0f;
	frame->IMUData[0].magData[2] = 3.0f;
	frame->cameraData = (uint8_t *)CoTaskMemAlloc(dscam_instance->GetWidth() * dscam_instance->GetHeight() * sizeof(uint8_t) * 3);

	//std::cout << "CameraData: " << &(frame->cameraData) << std::endl;
	//std::cout << "Size: " << frame->width * frame->height * sizeof(uint8_t) << std::endl;

	bool bUsing = false;

	
	while (instance != NULL)
	{


		bUsing = true;
		if (dscam_instance != NULL&&frame != NULL)
			dscam_instance->QueryFrame(frame->cameraData, dscam_instance->GetWidth() * dscam_instance->GetHeight() * sizeof(uint8_t) * 3);

		pData->callback(frame, pData->pUserData);
		
		bUsing = false;
	}

	ExitThread(0);
	return 0;
}

DWORD WINAPI HIDFrameFunction(LPVOID lpParam) {
	HANDLE hStdout;
	TCHAR msgBuf[MAX_STR];
	size_t cchStringSize;
	DWORD dwChars;

	//Make sure there is a console to receive output results
	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hStdout == INVALID_HANDLE_VALUE)
	{
		return 1;
	}

	if (cameron_info == NULL)
		allocCameronInfo(cameron_info);
	
	unsigned char buf[MAX_STR + 1];
	memset(buf, 0, sizeof(buf));
	int res = 0;
	//temp
	int tempStamp=0;

	while (hid_instance != NULL)
	{
		res = hid_read(hid_instance, buf, sizeof(buf));
		if (res == 0)
		{
			continue;
		}
		if (res < 0)
		{
			printf("read error");
			continue;
		}

		switch (buf[0])
		{
		case 0x01: {
			//controll package
			buf[MAX_STR] = '\0';
			DecodePackage(&buf[1], res);
			break;
		}
		case 0x02: {
			//data package
			AnalysisIMU(&buf[1], imuframe);
			imuframe->timeStamp = tempStamp;
			tempStamp++;
			//TODO callback
			//printf("recieved a echo %d", tempStamp);
			break;
		}
		default: {printf("recieved a error string"); break; }
		}

		memset(buf, 0, sizeof(buf));
	}

	ExitThread(0);
	return 0;
}

void ErrorHandler(LPTSTR lpszFunction)
{
	// Retrieve the system error message for the last-error code.

	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	// Display the error message.

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
	StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error %d: %s"),
		lpszFunction, dw, lpMsgBuf);
	MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

	// Free error-handling buffer allocations.

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}

bool SendPackage(int lenth, byte firstbyte,
	byte secondbyte,
	byte thirdbyte,
	byte forthbyte,
	byte fifthbyte,
	byte sixthbyte,
	byte seventhbyte,
	byte* content,
	int contentlenth) {

	if (!hid_instance) {
		ErrorHandler(TEXT("Can't Open HID device"));
		return false;
	}
	if (lenth < 0 || contentlenth < 0 || lenth < contentlenth + 5)
	{
		ErrorHandler(TEXT("send Lenth error"));
		return false;
	}
	int buflenth = SENT_LENGTH > lenth ? SENT_LENGTH : lenth;
	unsigned char* writeBuf = new unsigned char[buflenth];
	memset(writeBuf, 0, buflenth);
	EncodePackage(writeBuf, buflenth,
		firstbyte,
		secondbyte,
		thirdbyte,
		forthbyte,
		fifthbyte,
		sixthbyte,
		seventhbyte,
		content,
		contentlenth
	);

	hid_write(hid_instance, writeBuf, buflenth);
	Sleep(10);

}

//Encode HID echo data
bool EncodePackage(unsigned char *&package, int lenth,
	byte CAMERON_REQ_TYPE,
	byte CAMERON_REQ_TARGET,
	byte CAMERON_REQ_PURPOSE_DEVICE,
	byte LED_Param1,
	byte LED_Param2,
	byte LED_Param3,
	byte LED_Param4,
	byte* content,
	int content_lenth)
{
	unsigned char* tmpWrite = package;
	if (content_lenth > lenth || lenth < 0 || lenth > 64)
	{
		ErrorHandler(TEXT("encode package lenth error"));
		return false;
	}
	if (package == NULL)
	{
		ErrorHandler(TEXT("encode package buffer error"));
		return false;
	}
	int i = 0;
	//encode package
	{
		//report id
		if(CopyAndMove(tmpWrite, &REPORT_ID, sizeof(REPORT_ID))
			&&CopyAndMove(tmpWrite, &CAMERON_REQ_TYPE, sizeof(CAMERON_REQ_TYPE))
			&&CopyAndMove(tmpWrite, &CAMERON_REQ_TARGET, sizeof(CAMERON_REQ_TARGET))
			&&CopyAndMove(tmpWrite, &CAMERON_REQ_PURPOSE_DEVICE, sizeof(CAMERON_REQ_PURPOSE_DEVICE))
			&&CopyAndMove(tmpWrite, &LED_Param1, sizeof(LED_Param1))
			&&CopyAndMove(tmpWrite, &LED_Param2, sizeof(LED_Param2))
			&& CopyAndMove(tmpWrite, &LED_Param3, sizeof(LED_Param3))
			&& CopyAndMove(tmpWrite, &LED_Param4, sizeof(LED_Param4))
			&& content==NULL?true:CopyAndMove(tmpWrite, content, sizeof(content_lenth)))
		{
			tmpWrite = NULL;
			return true;
		}
		else
		{
			ErrorHandler("package form error");
			return false;
		}

	}



}

//Decode HID echo data
bool DecodePackage(unsigned char* package, int lenth)
{
	if (lenth < 0)
	{
		ErrorHandler(TEXT("decode package lenth error"));
		return false;
	}
	bool isError = false;
	//if error?
	switch (*package)
	{
	case RES_NO_ERROR: {
		isError = false;
		break;
	}
	case RES_ERROR: 
	{
		isError = true;
		ErrorHandler(TEXT("Set Fail")); 
		break; 
	}
	default: {
		ErrorHandler(TEXT("Receive Package Fail"));
		return false;
			break;
	}
	}

	if (!isError)
	{
		//right
		switch (*(package+1))
		{
		case   RES_DEVICE: {
			//CAMERON_RES_PURPOSE_DEVICE
			switch (*(package+2))
			{
			case REQ_OPEN: {break; }
			case REQ_CLOSE: {break; }
			case REQ_START: {break; }
			case REQ_STOP: {break; }
			case REQ_PARAMS_DEVICE_NAME: {
				if (cameron_info->device_name == NULL)
					allocCameronInfo(cameron_info);
				_memccpy(cameron_info->device_name,package+7, 0, sizeof(*(package+3)));
				break; }
			case REQ_PARAMS_SERIAL_NUM: {
				if (cameron_info->serial_number == NULL)
					allocCameronInfo(cameron_info);
				_memccpy(cameron_info->serial_number, package + 7, 0, sizeof(*(package + 3)));
				break; }
			case REQ_PARAMS_FW_VERSION: {
				if (cameron_info->firmware_version == NULL)
					allocCameronInfo(cameron_info);
				_memccpy(cameron_info->firmware_version, package + 7, 0, sizeof(*(package + 3)));
				break; }
			case REQ_PARAMS_FW_BUILD: {
				if (cameron_info->firmware_build == NULL)
					allocCameronInfo(cameron_info);
				_memccpy(cameron_info->firmware_build, package + 7, 0, sizeof(*(package + 3)));
				break; }
			case REQ_PARAMS_LED: {
				if (cameron_info == NULL)
					allocCameronInfo(cameron_info);
				cameron_info->led_on_time = *(package + 4);
				cameron_info->led_on_time = cameron_info->led_on_time << 8 | *(package + 3);
				cameron_info->led_off_time = *(package + 6);
				cameron_info->led_off_time = cameron_info->led_on_time << 8 | *(package + 5);
				break; }
			default: {
				ErrorHandler(TEXT("RES_DEVICE Purpose Error"));
				break;
			}
			}
			break;
		}
		case RES_DISPLAY: {
			//CAMERON_RES_PURPOSE_DISPLAY
			switch (*(package + 2))
			{
			case REQ_ON: {break; }
			case REQ_OFF: {break; }
			case REQ_STATUS: {
				//P1_DISPLAY_STATUS
				//Left 
				switch (*(package+3))
				{
				case LEFT_ON: {
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info); 
					cameron_info->display_left = true;
					break;
				}				
				case LEFT_OFF: {
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_left = false;
					break;
				}
				default:
				{
					ErrorHandler(TEXT("REQ_STATUS is error"));
					break;
				}
				}
				break; }
			case REQ_PADDING: {
				//Right or Left
				switch (*(package+3))
				{
				case LEFT_DISPLAY:
				{
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_padding.paddingLeftDisplay[0] = *(package + 4);
					cameron_info->display_padding.paddingLeftDisplay[1] = *(package + 5);
					break; }
				case RIGHT_DISPLAY: {
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_padding.paddingRightDisplay[0] = *(package + 4);
					cameron_info->display_padding.paddingRightDisplay[1] = *(package + 5);
					break; }
				default: {
					ErrorHandler(TEXT("PADDING Param1 error!"));
					break;
				}
				}
				break; }
			case REQ_BRIGHTNESS: {
				//Right or Left
				switch (*(package + 3))
				{
				case LEFT_DISPLAY:
				{
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_left_brightness_r = *(package + 4);
					cameron_info->display_left_brightness_g = *(package + 5);
					cameron_info->display_left_brightness_b = *(package + 6);

					break; }
				case RIGHT_DISPLAY: {
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_right_brightness_r = *(package + 4);
					cameron_info->display_right_brightness_g = *(package + 5);
					cameron_info->display_right_brightness_b = *(package + 6);
					break; }
				default: {
					ErrorHandler(TEXT("PADDING Param1 error!"));
					break;
				}
				}
				break;
				 }
			default: {
				ErrorHandler(TEXT("RES_DISPLAY Purpose Error"));
				break;
			}
			}
			break; }
		case RES_SENSOR_IMU: {/*TODO*/break; }
		case RES_SENSOR_ALS:{/*TODO*/break; }
		case RES_SENSOR_PXM: {/*TODO*/break; }
		default: {
			ErrorHandler(TEXT("Receiver data error"));
			break;
		}
		}
	}
	else
	{
		//Error
		switch (*(package + 1))
		{
		case   RES_DEVICE: {			
			switch (*(package + 2))
		{
		case REQ_OPEN: {break; }
		case REQ_CLOSE: {break; }
		case REQ_START: {break; }
		case REQ_STOP: {break; }
		case REQ_PARAMS_DEVICE_NAME: {
			ErrorHandler(TEXT("REQ_PARAMS_DEVICE_NAME Set Failed"));
			break; }
		case REQ_PARAMS_SERIAL_NUM: {
			ErrorHandler(TEXT("REQ_PARAMS_SERIAL_NUM Set Failed"));
			break; }
		case REQ_PARAMS_FW_VERSION: {
			ErrorHandler(TEXT("REQ_PARAMS_FW_VERSION Set Failed"));
			break; }
		case REQ_PARAMS_FW_BUILD: {
			ErrorHandler(TEXT("REQ_PARAMS_FW_BUILD Set Failed"));
			break; }
		case REQ_PARAMS_LED: {
			ErrorHandler(TEXT("REQ_PARAMS_LED Set Failed"));
			break; }
		default: {
			ErrorHandler(TEXT("RES_DEVICE Purpose Error"));
			break;
		}
		}
		break;
		}
		case RES_DISPLAY: {//CAMERON_RES_PURPOSE_DISPLAY
			switch (*(package + 2))
			{
			case REQ_ON: {break; }
			case REQ_OFF: {break; }
			case REQ_STATUS: {
					ErrorHandler(TEXT("SET RES_DISPLAY REQ_STATUS FAILED"));
					break; }
			case REQ_PADDING: {
				//Right or Left
				switch (*(package + 3))
				{
				case LEFT_DISPLAY:
				{
					ErrorHandler(TEXT("SET RES_DISPLAY REQ_PADDING LEFT FAILED"));
					break; }
				case RIGHT_DISPLAY: {
					ErrorHandler(TEXT("SET RES_DISPLAY REQ_PADDING RIGHT FAILED"));
					break; }
				default: {
					ErrorHandler(TEXT("PADDING Param1 error!"));
					break;
				}
				}
				break; }
			case REQ_BRIGHTNESS: {
				//Right or Left
				switch (*(package + 3))
				{
				case LEFT_DISPLAY:
				{
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_left_brightness_r = *(package + 4);
					cameron_info->display_left_brightness_g = *(package + 5);
					cameron_info->display_left_brightness_b = *(package + 6);

					break; }
				case RIGHT_DISPLAY: {
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_right_brightness_r = *(package + 4);
					cameron_info->display_right_brightness_g = *(package + 5);
					cameron_info->display_right_brightness_b = *(package + 6);
					break; }
				default: {
					ErrorHandler(TEXT("PADDING Param1 error!"));
					break;
				}
				}
				break;
			}
			default: {
				ErrorHandler(TEXT("RES_DISPLAY Purpose Error"));
				break;
			}
			}
			break; }
		case RES_SENSOR_IMU: {/*TODO*/break; }
		case RES_SENSOR_ALS: {/*TODO*/break; }
		case RES_SENSOR_PXM: {/*TODO*/break; }
		default: {
			ErrorHandler(TEXT("Receiver data error"));
			break;
		}
		}
	}


	return true;

}

//Decode IMU data 
void analysisMAG(uint8_t* src, int16_t* det)
{
	//x��y has 13significant
	int16_t tmp = 0;
	//x
	*src = *src >> 5;
	//sign bit
	tmp = (*det | *(src + 1) << 8) & 0x8000;
	*(src + 1) = *(src + 1) & 0x7F;
	*det = ((*det | *(src + 1)) << 3) | tmp | (*src);


	//y
	*(src + 2) = *(src + 2) >> 5;
	tmp = (*(det + 1) | *(src + 3) << 8) & 0x8000;
	*(src + 3) = *(src + 3) & 0x7F;
	*(det + 1) = (*(det + 1) | *(src + 3) << 3) | tmp | (*(src + 2));


	//z has 15 significant
	*(src + 4) = *(src + 4) >> 1;

	tmp = (*(det + 2) | *(src + 5) << 8) & 0x8000;
	*(src + 5) = *(src + 5) & 0x7F;

	*(det + 2) = (*(det + 2) | *(src + 5) << 7) | tmp | (*(src + 4));
}
void analysisGYR(uint8_t* src, int16_t* det)
{
	*det = (*det | *(src + 1)) << 8 | *src;
	*(det + 1) = (*(det + 1) | *(src + 3)) << 8 | *(src + 2);
	*(det + 2) = (*(det + 2) | *(src + 5)) << 8 | *(src + 4);
}
void analysisACC(uint8_t* src, int16_t* det)
{
	*det = (*det | *(src + 1)) << 8 | *src;
	*(det + 1) = (*(det + 1) | *(src + 3)) << 8 | *(src + 2);
	*(det + 2) = (*(det + 2) | *(src + 5)) << 8 | *(src + 4);
}

void AnalysisIMU(unsigned char* src_data, CameronIMUSample* des_data)
{
	uint8_t mag_src[6], gyr_src[6], acc_src[6];
	int16_t mag_des[3] = { 0 };
	int16_t gyr_des[3] = { 0 };
	int16_t acc_des[3] = { 0 };


	memcpy(mag_src, (src_data ), 6);
	memcpy(gyr_src, (src_data + 6 + 2), 6);
	memcpy(acc_src, (src_data + 6 + 2 + 6), 6);

	analysisMAG(mag_src, mag_des);
	analysisGYR(gyr_src, gyr_des);
	analysisACC(acc_src, acc_des);

	des_data->magData[0] = ((float)mag_des[0] / 0x7fff) * IMU_Mag_RANGE_XY;
	des_data->magData[1] = ((float)mag_des[1] / 0x7fff) * IMU_Mag_RANGE_XY;
	des_data->magData[2] = ((float)mag_des[2] / 0x7fff) * IMU_Mag_RANGE_Z;

	des_data->gyroData[0] = ((float)gyr_des[0] / 0x7fff)*IMU_GYRO_RANGE;
	des_data->gyroData[1] = ((float)gyr_des[1] / 0x7fff)*IMU_GYRO_RANGE;
	des_data->gyroData[2] = ((float)gyr_des[2] / 0x7fff)*IMU_GYRO_RANGE;

	des_data->accelData[0] = ((float)acc_des[0] / 0x7fff)*IMU_ACCL_RANGE;
	des_data->accelData[1] = ((float)acc_des[1] / 0x7fff)*IMU_ACCL_RANGE;
	des_data->accelData[2] = ((float)acc_des[2] / 0x7fff)*IMU_ACCL_RANGE;
}

bool allocCameronInfo(CameronInfo *&info)
{
	if (info == NULL)
		info = new CameronInfo();
	if (info->device_name == NULL)
		info->device_name = new byte[MAX_STR+1];
	if (info->device_version == NULL)
		info->device_version = new byte[MAX_STR + 1];
	if (info->firmware_build == NULL)
		info->firmware_build = new byte[MAX_STR + 1];
	if (info->firmware_version == NULL)
		info->firmware_version = new byte[MAX_STR + 1];
	if (info->manufacturer_string == NULL)
		info->manufacturer_string = new byte[MAX_STR + 1];
	if (info->product_string == NULL)
		info->product_string = new byte[MAX_STR + 1];
	if (info->serial_number == NULL)
		info->serial_number = new byte[MAX_STR + 1];
	
	return true;
}
bool freeCameronInfo(CameronInfo *&info)
{
	if (info == NULL)
		return true;


	if (info->device_name != NULL)
		delete	info->device_name;
	info->device_name = NULL;

	if (info->device_version != NULL)
		delete	info->device_version;
		info->device_version = NULL;

	if (info->firmware_build != NULL)
		delete	info->firmware_build;
		info->firmware_build = NULL;

	if (info->firmware_version != NULL)
		delete	info->firmware_version;
		info->firmware_version = NULL;

	if (info->manufacturer_string != NULL)
		delete	info->manufacturer_string;
		info->manufacturer_string = NULL;	

	if (info->product_string != NULL)
		delete	info->product_string;
		info->product_string = NULL;

	if (info->serial_number != NULL)
		delete	info->serial_number;
		info->serial_number = NULL;

	delete info;
	info = NULL;
	return true;
}


//utility 
bool wchar2char(wchar_t* &pwchar, char* pchar)
{
	if (pwchar == NULL || pchar == NULL)
		return false;
	size_t len = wcslen(pwchar) + 1;
	wcstombs_s(0, pchar, len, pwchar, _TRUNCATE);
	return true;
}

bool CopyAndMove(unsigned char *&pDst, const unsigned char* pSrc, int lenth) {
	if (pDst == NULL || pSrc == NULL || lenth < 0)
	{
		ErrorHandler(TEXT("memory copy error"));
		return false;
	}
	memcpy(pDst, pSrc, lenth);
	pDst += lenth;
	return true;
}

=======


#include <CameronDeviceLib.h>

#include <iostream>

#include <windows.h>
#include <tchar.h>
#include <strsafe.h>

//include DScamera
#include <UVCcamera\DScam.h>

//include HIDapi
#include <HID\hidapi.h>

#pragma comment(lib, "strmiids")

#pragma region Constance and Macro

#pragma region Send Package

const unsigned char REPORT_ID = 0x01;

//CAMERON_REQ_TYPE------Byte1
#define REQ_SET 0x00
#define REQ_GET 0x01

//CAMERON_REQ_TARGET------Byte2
#define REQ_DEVICE  0x00
#define REQ_DISPLAY  0x01
#define REQ_SENSOR_IMU  0X02
#define REQ_SENSOR_ALS  0X03
#define PEQ_SENSOR_PXM  0x04

//CAMERON_REQ_PURPOSE_DEVICE------Byte3
#define REQ_OPEN  0x00                
#define REQ_CLOSE  0x01                 
#define REQ_START  0x02                 
#define REQ_STOP  0x03            
#define REQ_PARAMS_DEVICE_NAME 0x04
#define REQ_PARAMS_SERIAL_NUM 0x05
#define REQ_PARAMS_FW_VERSION 0x06
#define REQ_PARAMS_FW_BUILD 0x07
#define REQ_PARAMS_LED 0x08

//CAMERON_REQ_PURPOSE_DISPLAY
#define REQ_ON 0x00
#define REQ_OFF 0x01
#define REQ_STATUS  0x02      
#define REQ_PADDING 0x03  
#define REQ_BRIGHTNESS 0x04

//Param1-----------Byte4
#define LED_ON_TIME  0x01 //0x00~0xFF
#define DISPLAY_LEFT 0X00
#define DISPLAY_RIGHT 0X01
//Param2-----------Byte5
#define LED_OFF_TIME  0x01//0x00~0xFF
//Param3-----------Byte6
//Param4-----------Byte7
#pragma endregion

#pragma region Receive Package

//ifError---------first byte
#define RES_NO_ERROR 0x00
#define RES_ERROR 0x01

//Receiver-----second byte
#define RES_DEVICE 0x00
#define RES_DISPLAY 0x01
#define RES_SENSOR_IMU 0x02
#define RES_SENSOR_ALS 0x03
#define RES_SENSOR_PXM 0x04

//Purpose-----third byte
//the same as send package

//Param1-----forth byte
#define LEFT_ON 0x00
#define LEFT_OFF 0x01

#define LEFT_DISPLAY 0x00
#define RIGHT_DISPLAY 0x01
//Param2-----fifth byte
#define RIGHT_ON 0x00
#define RIGHT_OFF 0x01
//Param3-----sixth byte
//Param4-----seventh byte

#pragma endregion

#define CAMERAONVID 0x0483
#define CAMERAONPID 0x5750
#define MAX_STR 255
#define SENT_LENGTH 64
#define IMU_Mag_RANGE_XY 1300
#define IMU_Mag_RANGE_Z 2500
#define IMU_GYRO_RANGE 500
#define IMU_GRAVITY 9.81
#define IMU_ACCL_RANGE (4*IMU_GRAVITY)



#pragma endregion

#pragma region Type Defind

typedef DScamera* DScamea_instance;

typedef hid_device* HID_instance;

typedef struct MyData {
	CameronInstance instance;
	CameronFrameCallback callback;
	void *pUserData;
} MYDATA, *PMYDATA;

#pragma endregion

#pragma region  Global variable

//DS camera
const char* DScameraName = "Logitech HD Pro Webcam C920";

//Instance
CameronInstance instance;
CameronFrameCallback callback = NULL;
DScamea_instance dscam_instance = NULL;
HID_instance hid_instance = NULL;
CameronFrame * frame = NULL;
CameronIMUSample *imuframe = NULL;
CameronInfo *cameron_info = NULL;
int dscamIndex = 0;

bool           m_bRunning = false;

// Sample custom data structure for threads to use.
// This is passed by void pointer so it can be any data type
// that can be passed using a single void pointer (LPVOID).

PMYDATA pData;
DWORD   dwThreadId;
HANDLE  hThread;
DWORD   dwHIDThreadId;
HANDLE  hHIDThread;

/* END - These elements are temporary stuff to test the Dashboard */


char* version = "0.1"; //this string is stored in this CPP
#pragma endregion

/* These elements are temporary stuff to test the Dashboard */

DWORD WINAPI CameronFrameFunction(LPVOID lpParam);
DWORD WINAPI HIDFrameFunction(LPVOID lpParam);
void ErrorHandler(LPTSTR lpszFunction);


//allocate and free the CameronInfo
bool allocCameronInfo(CameronInfo *&info);
bool freeCameronInfo(CameronInfo *&info);

//Send HID package
bool SendPackage(int lenth, byte firstbyte,
	byte secondbyte,
	byte thirdbyte,
	byte forthbyte=0x00,
	byte fifthbyte=0x00,
	byte sixthbyte=0x00,
	byte seventhbyte=0x00,
	byte* content=NULL,
	int contentlenth=0);

//Encode HID echo package
bool EncodePackage(unsigned char *&package, int lenth,
	byte CAMERON_REQ_TYPE,
	byte CAMERON_REQ_TARGET,
	byte CAMERON_REQ_PURPOSE_DEVICE,
	byte LED_Param1,
	byte LED_Param2,
	byte LED_Param3,
	byte LED_Param4,
	byte* content,
	int content_lenth);
//Decode HID echo package
bool DecodePackage(unsigned char* package, int lenth);
void AnalysisIMU(unsigned char* src_data, CameronIMUSample* des_data);
void analysisMAG(uint8_t* src, int16_t* det);
void analysisGYR(uint8_t* src, int16_t* det);
void analysisACC(uint8_t* src, int16_t* det);
//utility

//width char to char
bool wchar2char(wchar_t* &pwchar, char* pchar);
//copy memory to dest, and move the dest point to the end
bool CopyAndMove(unsigned char*& pDst, const unsigned char* pSrc, int lenth);

API_FUNCTION(char*) GetCameronDeviceLibVersion()
{
	return version;
}

API_FUNCTION(bool) OpenCameron(CameronInstance *&cameron)
{
	//Creates the local instance 
	cameron = new CameronInstance();
	instance = cameron;
	dscam_instance = new DScamera();
	frame = new CameronFrame();
	imuframe = new CameronIMUSample();
	if (hid_init() < 0)
	{
		ErrorHandler(TEXT("Hid Device initial error"));
		return false;
	}

	//OpenHID Device
	{
		hid_instance = hid_open(CAMERAONVID, CAMERAONPID, NULL);
		if (!hid_instance)
		{
			ErrorHandler(TEXT("HID device open fail"));
			return false;
		}
		// Set the hid_read() function to be non-blocking.
		hid_set_nonblocking(hid_instance, 1);
		
		hHIDThread = CreateThread(
			NULL,                   // default security attributes
			0,                      // use default stack size  
			HIDFrameFunction,       // thread function name
			pData,								// argument to thread function 
			0,                      // use default creation flags 
			&dwHIDThreadId);						// returns the thread identifier 

		if (hHIDThread == NULL)
			ErrorHandler(TEXT("Create HID Thread"));
	
	CloseHandle(hHIDThread);
	}

	//Get HID info
	{
		if (cameron_info == NULL)
			allocCameronInfo(cameron_info);

		GetCameronSerialNumber(instance, (char *)(cameron_info->serial_number), MAX_STR);
	}
	//open the connection to the MCU via USB
	{
	}

	return true;
}

API_FUNCTION(bool) CloseCameron(CameronInstance cameron)
{

	hid_close(hid_instance);

	hid_exit();

	hid_instance = NULL;

	if (dscam_instance != NULL)
		delete dscam_instance;

	dscam_instance = NULL;

	if (frame != NULL)
		delete frame;

	if (imuframe != NULL)
		delete imuframe;
	imuframe = NULL;

	freeCameronInfo(cameron_info);
	frame = NULL;
	//closes the connection to the MCU via USB
	instance = NULL;

	return true;
}

API_FUNCTION(bool) StartCameron(CameronInstance cameron, CameronFrameCallback frameCallback, void * pUserData)
{


	//tells cameron to start sending camera images, IMU and to turn on the displays
	callback = frameCallback;

	//temporary code for teting the CameronFrameViewer
	{
		std::cout << "Starting Thread" << std::endl;
		std::cout << "CameronInstance: " << instance << ", CameronCallback: " << callback << std::endl;

		pData = (PMYDATA)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(MYDATA));
		pData->callback = callback;
		pData->instance = instance;
		pData->pUserData = pUserData;

		//open Camera with name
		if (dscam_instance!=NULL
			&&DScamera::CameraCount() > 0 
			&& DScamera::CameraIndex(dscamIndex, DScameraName, strlen(DScameraName)))
		{
			dscam_instance->OpenCamera(dscamIndex, 1280, 720, 30, 0);
		}

		m_bRunning = true;

		hThread = CreateThread(
			NULL,                   // default security attributes
			0,                      // use default stack size  
			CameronFrameFunction,       // thread function name
			pData,								// argument to thread function 
			0,                      // use default creation flags 
			&dwThreadId);						// returns the thread identifier 

		if (hThread == NULL)
			ErrorHandler(TEXT("CreateThread"));
	}
	CloseHandle(hThread);
	return true;
}

API_FUNCTION(bool) StopCameron(CameronInstance cameron)
{
	//tells cameron to stop sending all kind of data
	instance = NULL;
	
	//temporary code for teting the CameronFrameViewer
	{
		WaitForSingleObject(hThread, INFINITE);
		CloseHandle(hThread);
	}
	dscam_instance->CloseCamera();
	return true;
}

API_FUNCTION(bool) UpdateFirmware(char * filename)
{
	//load firmware and send it to the device
	std::cout << "Firmware Filename: " << filename << std::endl;
	return true;
}

char * stringStoredInROM = "BRING NAME FROM ROM";
API_FUNCTION(bool) GetCameronDeviceName(CameronInstance cameron, char * val, int len)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DEVICE,
		REQ_PARAMS_DEVICE_NAME);

	if (cameron_info->device_name != NULL) {
		_memccpy(val, cameron_info->device_name, 0, len);
		return true;
	}
	_memccpy(val, "get device error", 0, len);
	return false;
}

API_FUNCTION(bool) GetCameronSerialNumber(CameronInstance cameron, char * val, int len)
{
	if (!hid_instance) {
		ErrorHandler(TEXT("Can't Open HID device"));
		return false;
	}

	wchar_t* wstr = new wchar_t(len/ 2);

	int res = hid_get_serial_number_string(hid_instance,wstr, (len / 2 > MAX_STR / 2) ? MAX_STR / 2 : len / 2);


	if (res < 0)
	{
		ErrorHandler(TEXT("Get Serial Number Error"));
		_memccpy(val, "Get Serial Number Error1", 0, len);
		return false;
	}

	//_memccpy(val, stringStoredInROM, 0, len);


	return wchar2char(wstr,val);

}

API_FUNCTION(bool) GetCameronFirmwareVersion(CameronInstance cameron, char * val, int len)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DEVICE,
		REQ_PARAMS_FW_VERSION);

	if (cameron_info->firmware_version != NULL) {
		_memccpy(val, cameron_info->firmware_version, 0, len);
		return true;
	}
	_memccpy(val, "get FirmwareVersion error", 0, len);
	return false;
}

API_FUNCTION(bool) GetCameronFirmwareBuild(CameronInstance cameron, char * val, int len)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DEVICE,
		REQ_PARAMS_FW_BUILD);

	if (cameron_info->firmware_build != NULL) {
		_memccpy(val, cameron_info->firmware_build, 0, len);
		return true;
	}
	_memccpy(val, "get firmware_build error", 0, len);
	return false;
}

API_FUNCTION(bool) GetCameronResolutionInfo(CameronInstance cameron, CameronResolutionInfo *info)
{
	if (cameron == NULL)
	{
		info->width = 1280;
		info->height = 720;
		info->fps = 30;
	}
	else
	{
		//queries the MCU for available resolutions
	}
	return true;
}

API_FUNCTION(bool) GetCameronExposure(CameronInstance cameron, float * val)
{
	//bring value from MCU
	(*val) = 123.0f;
	return true;
}

API_FUNCTION(bool) GetCameronExposureMS(CameronInstance cameron, float * val)
{
	//bring value from MCU
	(*val) = 432.0f;
	return true;
}

API_FUNCTION(bool) GetCameronAutoExposure(CameronInstance cameron, bool * val)
{
	//bring value from MCU
	(*val) = true;
	return true;
}

API_FUNCTION(bool) GetCameronGain(CameronInstance cameron, float * val)
{
	//bring value from MCU
	(*val) = 333.0f;
	return true;
}

API_FUNCTION(bool) GetCameronHFlip(CameronInstance cameron, bool * val)
{
	//bring value from MCU
	(*val) = true;
	return true;
}

API_FUNCTION(bool) GetCameronVFlip(CameronInstance cameron, bool * val)
{
	//bring value from MCU
	(*val) = true;
	return true;
}

API_FUNCTION(bool) GetCameronCalibrationPresent(CameronInstance cameron, bool * val)
{
	//bring value from MCU
	(*val) = true;
	return true;
}

API_FUNCTION(bool) GetCameronFOV(CameronInstance cameron, float * val)
{
	//bring values from MCU
	val[0] = 111.0f;
	val[1] = 222.0f;
	return true;
}

API_FUNCTION(bool) GetCameronRectifiedFOV(CameronInstance cameron, float * val)
{
	//bring values from MCU
	val[0] = 333.0f;
	val[1] = 444.0f;
	return true;
}

API_FUNCTION(bool) GetCameronUndistort(CameronInstance cameron, bool * val)
{
	//bring value from MCU
	(*val) = true;
	return true;
}

API_FUNCTION(bool) GetCameronIntrinsics(CameronInstance cameron, Cameron_INTR * val)
{
	//bring values from MCU
	val->width = 1280;
	val->height = 720;

	val->camera.cx = 1;
	val->camera.cy = 2;
	val->camera.fx = 3;
	val->camera.fy = 4;
	val->camera.p1 = 5;
	val->camera.p2 = 6;
	val->camera.k1 = 7;
	val->camera.k2 = 8;
	val->camera.k3 = 9;
	val->camera.k4 = 10;
	val->camera.k5 = 11;
	val->camera.k6 = 12;

	return true;
}

API_FUNCTION(bool) GetCameronExtrinsics(CameronInstance cameron, Cameron_EXTR * val)
{
	//bring values from MCU
	val->rotation[0] = 6.0f;
	val->rotation[1] = 5.0f;
	val->rotation[2] = 4.0f;
	val->translation[0] = 3.0f;
	val->translation[1] = 2.0f;
	val->translation[2] = 1.0f;

	return true;
}

API_FUNCTION(bool) GetCameronStatusLED(CameronInstance cameron, unsigned short * val)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DEVICE,
		REQ_PARAMS_LED);

	//bring values from MCU
	val[0] = cameron_info->led_on_time;
	val[1] = cameron_info->led_off_time;

	return true;
}

API_FUNCTION(bool) GetCameronDisplayStatus(CameronInstance cameron, bool *val)
{

	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DISPLAY,
		REQ_STATUS);


	//bring values from MCU
	val[0] = cameron_info->display_left;
	val[1] = cameron_info->display_right;

	return true;
}

API_FUNCTION(bool) GetCameronDisplayBrightness(CameronInstance cameron, unsigned char * val)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DISPLAY,
		REQ_BRIGHTNESS);


	//bring values from MCU
	val[0] = cameron_info->display_left_brightness_r;
	val[1] = cameron_info->display_left_brightness_g;
	val[2] = cameron_info->display_left_brightness_b;

	val[3] = cameron_info->display_right_brightness_r;
	val[4] = cameron_info->display_right_brightness_g;
	val[5] = cameron_info->display_right_brightness_b;

	return true;
}

API_FUNCTION(bool) GetCameronDisplayPadding(CameronInstance cameron, CameronDisplayCalibration * val)
{
	SendPackage(SENT_LENGTH,
		REQ_GET,
		REQ_DISPLAY,
		REQ_PADDING);



	//bring values from MCU
	val->paddingLeftDisplay[0] = cameron_info->display_padding.paddingLeftDisplay[0]; //left
	val->paddingLeftDisplay[1] = cameron_info->display_padding.paddingLeftDisplay[1]; //top
	val->paddingRightDisplay[0] = cameron_info->display_padding.paddingRightDisplay[0]; //left
	val->paddingRightDisplay[1] = cameron_info->display_padding.paddingRightDisplay[1]; //top

	return true;
}

API_FUNCTION(bool) GetCameronIMUCalibration(CameronInstance cameron, CameronIMU_Calibration * val)
{
	//bring values from MCU
	val->meanTimeDelta = 77788; //ms

	val->gravityDirection[0] = 999.0f;// unit vector
	val->gravityDirection[1] = 998.0f;// unit vector
	val->gravityDirection[2] = 997.0f;// unit vector

	val->magnetometerDirection[0] = 989.0f;// unit vector
	val->magnetometerDirection[1] = 988.0f;// unit vector
	val->magnetometerDirection[2] = 987.0f;// unit vector

	val->accelerometerVariance[0] = 979.0f;// g-units^2
	val->accelerometerVariance[1] = 978.0f;// g-units^2
	val->accelerometerVariance[2] = 977.0f;// g-units^2

	val->accelerometerDrift[0] = 969.0f;// g-units/s
	val->accelerometerDrift[1] = 968.0f;// g-units/s
	val->accelerometerDrift[2] = 967.0f;// g-units/s

	val->gyroVariance[0] = 959.0f;// (rad/s)^2
	val->gyroVariance[1] = 958.0f;// (rad/s)^2
	val->gyroVariance[2] = 957.0f;// (rad/s)^2

	val->gyroDrift[0] = 949.0f;// rad/s
	val->gyroDrift[1] = 948.0f;// rad/s
	val->gyroDrift[2] = 947.0f;// rad/s

	val->magnetometerVariance[0] = 939.0f;// units^2
	val->magnetometerVariance[1] = 938.0f;// units^2
	val->magnetometerVariance[2] = 937.0f;// units^2

	val->magnetometerDrift[0] = 929.0f;// units^2
	val->magnetometerDrift[1] = 928.0f;// units^2
	val->magnetometerDrift[2] = 927.0f;// units^2

	return true;
}

API_FUNCTION(bool) SetCameronResolutionInfo(CameronInstance cameron, CameronResolutionInfo resInfo)
{
	//send values to MCU
	std::cout << "Width: " << resInfo.width << ", Height: " << resInfo.height << ", FPS: " << resInfo.fps << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronExposure(CameronInstance cameron, float val)
{
	//send values to MCU
	std::cout << "Exposure: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronExposureMS(CameronInstance cameron, float val)
{
	//send values to MCU
	std::cout << "Exposure MS: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronAutoExposure(CameronInstance cameron, bool val)
{
	//send values to MCU
	std::cout << "Auto Exposure: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronGain(CameronInstance cameron, float val)
{
	//send values to MCU
	std::cout << "Gain: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronHFlip(CameronInstance cameron, bool val)
{
	//send values to MCU
	std::cout << "Horizontal Flip: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronVFlip(CameronInstance cameron, bool val)
{
	//send values to MCU
	std::cout << "Vertical Flip: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronUndistort(CameronInstance cameron, bool val)
{
	//send values to MCU
	std::cout << "Undistort: " << val << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronDeviceName(CameronInstance cameron, const char * val)
{
	if (strlen(val) > 56)
		return false;

	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DEVICE,
		REQ_PARAMS_DEVICE_NAME,
		(byte)strlen(val),
		0x00,
		0x00,
		0x00,
		(byte*)val,
		strlen(val));

	//TODO

	return true;
}

API_FUNCTION(bool) SetCameronSerialNumber(CameronInstance cameron, const char * val)
{
	if (strlen(val) > 56)
		return false;

	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DEVICE,
		REQ_PARAMS_SERIAL_NUM,
		(byte)strlen(val),
		0x00,
		0x00,
		0x00,
		(byte*)val,
		strlen(val));


	//TODO

	return true;
}

API_FUNCTION(bool) SetCameronFOV(CameronInstance cameron, float * val)
{
	//send values to MCU
	std::cout << "Horizontal FOV: " << val[0] << ", Vertical FOV: " << val[1] << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronRectifiedFOV(CameronInstance cameron, float * val)
{
	//send values to MCU
	std::cout << "Horizontal Rectified FOV: " << val[0] << ", Vertical Rectified FOV: " << val[1] << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronIntrinsics(CameronInstance cameron, Cameron_INTR val)
{
	//send values to MCU
	std::cout << "Intrinsics: " << std::endl;
	std::cout << " - Width: " << val.width << std::endl;
	std::cout << " - Height: " << val.height << std::endl;
	std::cout << " - cx: " << val.camera.cx << std::endl;
	std::cout << " - cy: " << val.camera.cy << std::endl;
	std::cout << " - fx: " << val.camera.fx << std::endl;
	std::cout << " - fy: " << val.camera.fy << std::endl;
	std::cout << " - k1: " << val.camera.k1 << std::endl;
	std::cout << " - k2: " << val.camera.k2 << std::endl;
	std::cout << " - k3: " << val.camera.k3 << std::endl;
	std::cout << " - k4: " << val.camera.k4 << std::endl;
	std::cout << " - k5: " << val.camera.k5 << std::endl;
	std::cout << " - k6: " << val.camera.k6 << std::endl;
	std::cout << " - p1: " << val.camera.p1 << std::endl;
	std::cout << " - p2: " << val.camera.p2 << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronExtrinsics(CameronInstance cameron, Cameron_EXTR val)
{
	//send values to MCU
	std::cout << "Extrinsics: " << std::endl;
	std::cout << " - rotationX: " << val.rotation[0] << std::endl;
	std::cout << " - rotationY: " << val.rotation[1] << std::endl;
	std::cout << " - rotationZ: " << val.rotation[2] << std::endl;
	std::cout << " - translationX: " << val.translation[0] << std::endl;
	std::cout << " - translationY: " << val.translation[1] << std::endl;
	std::cout << " - translationZ: " << val.translation[2] << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronStatusLED(CameronInstance cameron, unsigned short * val)
{
	byte* pval = (byte*)val;
	
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DEVICE,
		REQ_PARAMS_LED,
		*pval,
		*(pval+1),
		*(pval + 2),
		*(pval + 3),
		NULL,
		0);


	//TODO

	return true;
}

API_FUNCTION(bool) SetCameronDisplayStatus(CameronInstance cameron, bool * val)
{
	//todo
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DISPLAY,
		REQ_STATUS,
		*(byte*)val,
		*((byte*)val+1)
		);

	//send values to MCU

	return true;
}

API_FUNCTION(bool) SetCameronDisplayBrightness(CameronInstance cameron, unsigned char * val)
{
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DISPLAY,
		REQ_BRIGHTNESS,
		DISPLAY_LEFT,
		*(byte*)val,
		*((byte*)val + 1),
		*((byte*)val + 2)
	);
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DISPLAY,
		REQ_BRIGHTNESS,
		DISPLAY_RIGHT,
		*(byte*)val,
		*((byte*)val + 1),
		*((byte*)val + 2)
	);

	//send values to MCU
	//std::cout << "Left Display Brightness: " << val[0] << ", Right Display Brightness: " << val[1] << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronDisplayPadding(CameronInstance cameron, CameronDisplayCalibration val)
{
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DISPLAY,
		REQ_PADDING,
		DISPLAY_LEFT,
		val.paddingLeftDisplay[0],
		val.paddingLeftDisplay[1]
	);
	SendPackage(SENT_LENGTH,
		REQ_SET,
		REQ_DISPLAY,
		REQ_PADDING,
		DISPLAY_RIGHT,
		val.paddingRightDisplay[0],
		val.paddingRightDisplay[1]
	);


	//send values to MCU
	std::cout << "Display Padding: " << std::endl;
	std::cout << " - Left Display Left: " << val.paddingLeftDisplay[0] << ", Left Display Top: " << val.paddingLeftDisplay[1] << std::endl;
	std::cout << " - Right Display Left: " << val.paddingRightDisplay[0] << ", Right Display Top: " << val.paddingRightDisplay[1] << std::endl;

	return true;
}

API_FUNCTION(bool) SetCameronIMUCalibration(CameronInstance cameron, CameronIMU_Calibration val)
{
	//send values to MCU
	std::cout << "IMU Calibration: " << std::endl;
	std::cout << " - meanTimeDelta: " << val.meanTimeDelta << std::endl;

	std::cout << " - gravityDirectionX: " << val.gravityDirection[0] << std::endl;
	std::cout << " - gravityDirectionY: " << val.gravityDirection[1] << std::endl;
	std::cout << " - gravityDirectionZ: " << val.gravityDirection[2] << std::endl;

	std::cout << " - magnetometerDirectionX: " << val.magnetometerDirection[0] << std::endl;
	std::cout << " - magnetometerDirectionY: " << val.magnetometerDirection[1] << std::endl;
	std::cout << " - magnetometerDirectionZ: " << val.magnetometerDirection[2] << std::endl;

	std::cout << " - accelerometerVarianceX: " << val.accelerometerVariance[0] << std::endl;
	std::cout << " - accelerometerVarianceY: " << val.accelerometerVariance[1] << std::endl;
	std::cout << " - accelerometerVarianceZ: " << val.accelerometerVariance[2] << std::endl;

	std::cout << " - accelerometerDriftX: " << val.accelerometerDrift[0] << std::endl;
	std::cout << " - accelerometerDriftY: " << val.accelerometerDrift[1] << std::endl;
	std::cout << " - accelerometerDriftZ: " << val.accelerometerDrift[2] << std::endl;

	std::cout << " - gyroVarianceX: " << val.gyroVariance[1] << std::endl;
	std::cout << " - gyroVarianceY: " << val.gyroVariance[2] << std::endl;
	std::cout << " - gyroVarianceZ: " << val.gyroVariance[0] << std::endl;

	std::cout << " - gyroDriftX: " << val.gyroDrift[0] << std::endl;
	std::cout << " - gyroDriftY: " << val.gyroDrift[1] << std::endl;
	std::cout << " - gyroDriftZ: " << val.gyroDrift[2] << std::endl;

	std::cout << " - magnetometerVarianceX: " << val.magnetometerVariance[0] << std::endl;
	std::cout << " - magnetometerVarianceY: " << val.magnetometerVariance[1] << std::endl;
	std::cout << " - magnetometerVarianceZ: " << val.magnetometerVariance[2] << std::endl;

	std::cout << " - magnetometerDriftX: " << val.magnetometerDrift[0] << std::endl;
	std::cout << " - magnetometerDriftY: " << val.magnetometerDrift[1] << std::endl;
	std::cout << " - magnetometerDriftZ: " << val.magnetometerDrift[2] << std::endl;

	return true;
}

//----------------------- This is to test that a background thread can update the image -----------------------///

DWORD WINAPI CameronFrameFunction(LPVOID lpParam)
{
	HANDLE hStdout;
	PMYDATA pData;

	TCHAR msgBuf[250];
	size_t cchStringSize;
	DWORD dwChars;


	// Make sure there is a console to receive output results. 

	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hStdout == INVALID_HANDLE_VALUE)
		return 1;

	// Cast the parameter to the correct data type.
	// The pointer is known to be valid because 
	// it was checked for NULL before the thread was created.
	pData = (PMYDATA)lpParam;

	// Print the parameter values using thread-safe functions.
	std::cout << "CameronInstance: " << pData->instance << ", CameronCallback: " << pData->callback << std::endl;

	frame->timeStamp = 5678;
	frame->width = 320;
	frame->height = 180;
	frame->lightSensor = 9999;
	frame->proximitySensor = 8888;
	frame->IMUSamplesCount = 1;
	frame->IMUData[0].timeStamp = 5678;
	frame->IMUData[0].accelData[0] = 1.0f;
	frame->IMUData[0].accelData[1] = 1.0f;
	frame->IMUData[0].accelData[2] = 1.0f;
	frame->IMUData[0].gyroData[0] = 2.0f;
	frame->IMUData[0].gyroData[1] = 2.0f;
	frame->IMUData[0].gyroData[2] = 2.0f;
	frame->IMUData[0].magData[0] = 3.0f;
	frame->IMUData[0].magData[1] = 3.0f;
	frame->IMUData[0].magData[2] = 3.0f;
	frame->cameraData = (uint8_t *)CoTaskMemAlloc(dscam_instance->GetWidth() * dscam_instance->GetHeight() * sizeof(uint8_t) * 3);

	//std::cout << "CameraData: " << &(frame->cameraData) << std::endl;
	//std::cout << "Size: " << frame->width * frame->height * sizeof(uint8_t) << std::endl;

	bool bUsing = false;

	
	while (instance != NULL)
	{


		bUsing = true;
		if (dscam_instance != NULL&&frame != NULL)
			dscam_instance->QueryFrame(frame->cameraData, dscam_instance->GetWidth() * dscam_instance->GetHeight() * sizeof(uint8_t) * 3);

		pData->callback(frame, pData->pUserData);
		
		bUsing = false;
	}

	ExitThread(0);
	return 0;
}

DWORD WINAPI HIDFrameFunction(LPVOID lpParam) {
	HANDLE hStdout;
	TCHAR msgBuf[MAX_STR];
	size_t cchStringSize;
	DWORD dwChars;

	//Make sure there is a console to receive output results
	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hStdout == INVALID_HANDLE_VALUE)
	{
		return 1;
	}

	if (cameron_info == NULL)
		allocCameronInfo(cameron_info);
	
	unsigned char buf[MAX_STR + 1];
	memset(buf, 0, sizeof(buf));
	int res = 0;
	//temp
	int tempStamp=0;

	while (hid_instance != NULL)
	{
		res = hid_read(hid_instance, buf, sizeof(buf));
		if (res == 0)
		{
			continue;
		}
		if (res < 0)
		{
			printf("read error");
			continue;
		}

		switch (buf[0])
		{
		case 0x01: {
			//controll package
			buf[MAX_STR] = '\0';
			DecodePackage(&buf[1], res);
			break;
		}
		case 0x02: {
			//data package
			AnalysisIMU(&buf[1], imuframe);
			imuframe->timeStamp = tempStamp;
			tempStamp++;
			//TODO callback
			//printf("recieved a echo %d", tempStamp);
			break;
		}
		default: {printf("recieved a error string"); break; }
		}

		memset(buf, 0, sizeof(buf));
	}

	ExitThread(0);
	return 0;
}

void ErrorHandler(LPTSTR lpszFunction)
{
	// Retrieve the system error message for the last-error code.

	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	// Display the error message.

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
	StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error %d: %s"),
		lpszFunction, dw, lpMsgBuf);
	MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

	// Free error-handling buffer allocations.

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}

bool SendPackage(int lenth, byte firstbyte,
	byte secondbyte,
	byte thirdbyte,
	byte forthbyte,
	byte fifthbyte,
	byte sixthbyte,
	byte seventhbyte,
	byte* content,
	int contentlenth) {

	if (!hid_instance) {
		ErrorHandler(TEXT("Can't Open HID device"));
		return false;
	}
	if (lenth < 0 || contentlenth < 0 || lenth < contentlenth + 5)
	{
		ErrorHandler(TEXT("send Lenth error"));
		return false;
	}
	int buflenth = SENT_LENGTH > lenth ? SENT_LENGTH : lenth;
	unsigned char* writeBuf = new unsigned char[buflenth];
	memset(writeBuf, 0, buflenth);
	EncodePackage(writeBuf, buflenth,
		firstbyte,
		secondbyte,
		thirdbyte,
		forthbyte,
		fifthbyte,
		sixthbyte,
		seventhbyte,
		content,
		contentlenth
	);

	hid_write(hid_instance, writeBuf, buflenth);
	Sleep(10);

}

//Encode HID echo data
bool EncodePackage(unsigned char *&package, int lenth,
	byte CAMERON_REQ_TYPE,
	byte CAMERON_REQ_TARGET,
	byte CAMERON_REQ_PURPOSE_DEVICE,
	byte LED_Param1,
	byte LED_Param2,
	byte LED_Param3,
	byte LED_Param4,
	byte* content,
	int content_lenth)
{
	unsigned char* tmpWrite = package;
	if (content_lenth > lenth || lenth < 0 || lenth > 64)
	{
		ErrorHandler(TEXT("encode package lenth error"));
		return false;
	}
	if (package == NULL)
	{
		ErrorHandler(TEXT("encode package buffer error"));
		return false;
	}
	int i = 0;
	//encode package
	{
		//report id
		if(CopyAndMove(tmpWrite, &REPORT_ID, sizeof(REPORT_ID))
			&&CopyAndMove(tmpWrite, &CAMERON_REQ_TYPE, sizeof(CAMERON_REQ_TYPE))
			&&CopyAndMove(tmpWrite, &CAMERON_REQ_TARGET, sizeof(CAMERON_REQ_TARGET))
			&&CopyAndMove(tmpWrite, &CAMERON_REQ_PURPOSE_DEVICE, sizeof(CAMERON_REQ_PURPOSE_DEVICE))
			&&CopyAndMove(tmpWrite, &LED_Param1, sizeof(LED_Param1))
			&&CopyAndMove(tmpWrite, &LED_Param2, sizeof(LED_Param2))
			&& CopyAndMove(tmpWrite, &LED_Param3, sizeof(LED_Param3))
			&& CopyAndMove(tmpWrite, &LED_Param4, sizeof(LED_Param4))
			&& content==NULL?true:CopyAndMove(tmpWrite, content, sizeof(content_lenth)))
		{
			tmpWrite = NULL;
			return true;
		}
		else
		{
			ErrorHandler("package form error");
			return false;
		}

	}



}

//Decode HID echo data
bool DecodePackage(unsigned char* package, int lenth)
{
	if (lenth < 0)
	{
		ErrorHandler(TEXT("decode package lenth error"));
		return false;
	}
	bool isError = false;
	//if error?
	switch (*package)
	{
	case RES_NO_ERROR: {
		isError = false;
		break;
	}
	case RES_ERROR: 
	{
		isError = true;
		ErrorHandler(TEXT("Set Fail")); 
		break; 
	}
	default: {
		ErrorHandler(TEXT("Receive Package Fail"));
		return false;
			break;
	}
	}

	if (!isError)
	{
		//right
		switch (*(package+1))
		{
		case   RES_DEVICE: {
			//CAMERON_RES_PURPOSE_DEVICE
			switch (*(package+2))
			{
			case REQ_OPEN: {break; }
			case REQ_CLOSE: {break; }
			case REQ_START: {break; }
			case REQ_STOP: {break; }
			case REQ_PARAMS_DEVICE_NAME: {
				if (cameron_info->device_name == NULL)
					allocCameronInfo(cameron_info);
				_memccpy(cameron_info->device_name,package+7, 0, sizeof(*(package+3)));
				break; }
			case REQ_PARAMS_SERIAL_NUM: {
				if (cameron_info->serial_number == NULL)
					allocCameronInfo(cameron_info);
				_memccpy(cameron_info->serial_number, package + 7, 0, sizeof(*(package + 3)));
				break; }
			case REQ_PARAMS_FW_VERSION: {
				if (cameron_info->firmware_version == NULL)
					allocCameronInfo(cameron_info);
				_memccpy(cameron_info->firmware_version, package + 7, 0, sizeof(*(package + 3)));
				break; }
			case REQ_PARAMS_FW_BUILD: {
				if (cameron_info->firmware_build == NULL)
					allocCameronInfo(cameron_info);
				_memccpy(cameron_info->firmware_build, package + 7, 0, sizeof(*(package + 3)));
				break; }
			case REQ_PARAMS_LED: {
				if (cameron_info == NULL)
					allocCameronInfo(cameron_info);
				cameron_info->led_on_time = *(package + 4);
				cameron_info->led_on_time = cameron_info->led_on_time << 8 | *(package + 3);
				cameron_info->led_off_time = *(package + 6);
				cameron_info->led_off_time = cameron_info->led_on_time << 8 | *(package + 5);
				break; }
			default: {
				ErrorHandler(TEXT("RES_DEVICE Purpose Error"));
				break;
			}
			}
			break;
		}
		case RES_DISPLAY: {
			//CAMERON_RES_PURPOSE_DISPLAY
			switch (*(package + 2))
			{
			case REQ_ON: {break; }
			case REQ_OFF: {break; }
			case REQ_STATUS: {
				//P1_DISPLAY_STATUS
				//Left 
				switch (*(package+3))
				{
				case LEFT_ON: {
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info); 
					cameron_info->display_left = true;
					break;
				}				
				case LEFT_OFF: {
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_left = false;
					break;
				}
				default:
				{
					ErrorHandler(TEXT("REQ_STATUS is error"));
					break;
				}
				}
				break; }
			case REQ_PADDING: {
				//Right or Left
				switch (*(package+3))
				{
				case LEFT_DISPLAY:
				{
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_padding.paddingLeftDisplay[0] = *(package + 4);
					cameron_info->display_padding.paddingLeftDisplay[1] = *(package + 5);
					break; }
				case RIGHT_DISPLAY: {
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_padding.paddingRightDisplay[0] = *(package + 4);
					cameron_info->display_padding.paddingRightDisplay[1] = *(package + 5);
					break; }
				default: {
					ErrorHandler(TEXT("PADDING Param1 error!"));
					break;
				}
				}
				break; }
			case REQ_BRIGHTNESS: {
				//Right or Left
				switch (*(package + 3))
				{
				case LEFT_DISPLAY:
				{
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_left_brightness_r = *(package + 4);
					cameron_info->display_left_brightness_g = *(package + 5);
					cameron_info->display_left_brightness_b = *(package + 6);

					break; }
				case RIGHT_DISPLAY: {
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_right_brightness_r = *(package + 4);
					cameron_info->display_right_brightness_g = *(package + 5);
					cameron_info->display_right_brightness_b = *(package + 6);
					break; }
				default: {
					ErrorHandler(TEXT("PADDING Param1 error!"));
					break;
				}
				}
				break;
				 }
			default: {
				ErrorHandler(TEXT("RES_DISPLAY Purpose Error"));
				break;
			}
			}
			break; }
		case RES_SENSOR_IMU: {/*TODO*/break; }
		case RES_SENSOR_ALS:{/*TODO*/break; }
		case RES_SENSOR_PXM: {/*TODO*/break; }
		default: {
			ErrorHandler(TEXT("Receiver data error"));
			break;
		}
		}
	}
	else
	{
		//Error
		switch (*(package + 1))
		{
		case   RES_DEVICE: {			
			switch (*(package + 2))
		{
		case REQ_OPEN: {break; }
		case REQ_CLOSE: {break; }
		case REQ_START: {break; }
		case REQ_STOP: {break; }
		case REQ_PARAMS_DEVICE_NAME: {
			ErrorHandler(TEXT("REQ_PARAMS_DEVICE_NAME Set Failed"));
			break; }
		case REQ_PARAMS_SERIAL_NUM: {
			ErrorHandler(TEXT("REQ_PARAMS_SERIAL_NUM Set Failed"));
			break; }
		case REQ_PARAMS_FW_VERSION: {
			ErrorHandler(TEXT("REQ_PARAMS_FW_VERSION Set Failed"));
			break; }
		case REQ_PARAMS_FW_BUILD: {
			ErrorHandler(TEXT("REQ_PARAMS_FW_BUILD Set Failed"));
			break; }
		case REQ_PARAMS_LED: {
			ErrorHandler(TEXT("REQ_PARAMS_LED Set Failed"));
			break; }
		default: {
			ErrorHandler(TEXT("RES_DEVICE Purpose Error"));
			break;
		}
		}
		break;
		}
		case RES_DISPLAY: {//CAMERON_RES_PURPOSE_DISPLAY
			switch (*(package + 2))
			{
			case REQ_ON: {break; }
			case REQ_OFF: {break; }
			case REQ_STATUS: {
					ErrorHandler(TEXT("SET RES_DISPLAY REQ_STATUS FAILED"));
					break; }
			case REQ_PADDING: {
				//Right or Left
				switch (*(package + 3))
				{
				case LEFT_DISPLAY:
				{
					ErrorHandler(TEXT("SET RES_DISPLAY REQ_PADDING LEFT FAILED"));
					break; }
				case RIGHT_DISPLAY: {
					ErrorHandler(TEXT("SET RES_DISPLAY REQ_PADDING RIGHT FAILED"));
					break; }
				default: {
					ErrorHandler(TEXT("PADDING Param1 error!"));
					break;
				}
				}
				break; }
			case REQ_BRIGHTNESS: {
				//Right or Left
				switch (*(package + 3))
				{
				case LEFT_DISPLAY:
				{
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_left_brightness_r = *(package + 4);
					cameron_info->display_left_brightness_g = *(package + 5);
					cameron_info->display_left_brightness_b = *(package + 6);

					break; }
				case RIGHT_DISPLAY: {
					if (cameron_info == NULL)
						allocCameronInfo(cameron_info);
					cameron_info->display_right_brightness_r = *(package + 4);
					cameron_info->display_right_brightness_g = *(package + 5);
					cameron_info->display_right_brightness_b = *(package + 6);
					break; }
				default: {
					ErrorHandler(TEXT("PADDING Param1 error!"));
					break;
				}
				}
				break;
			}
			default: {
				ErrorHandler(TEXT("RES_DISPLAY Purpose Error"));
				break;
			}
			}
			break; }
		case RES_SENSOR_IMU: {/*TODO*/break; }
		case RES_SENSOR_ALS: {/*TODO*/break; }
		case RES_SENSOR_PXM: {/*TODO*/break; }
		default: {
			ErrorHandler(TEXT("Receiver data error"));
			break;
		}
		}
	}


	return true;

}

//Decode IMU data 
void analysisMAG(uint8_t* src, int16_t* det)
{
	//x��y has 13significant
	int16_t tmp = 0;
	//x
	*src = *src >> 5;
	//sign bit
	tmp = (*det | *(src + 1) << 8) & 0x8000;
	*(src + 1) = *(src + 1) & 0x7F;
	*det = ((*det | *(src + 1)) << 3) | tmp | (*src);


	//y
	*(src + 2) = *(src + 2) >> 5;
	tmp = (*(det + 1) | *(src + 3) << 8) & 0x8000;
	*(src + 3) = *(src + 3) & 0x7F;
	*(det + 1) = (*(det + 1) | *(src + 3) << 3) | tmp | (*(src + 2));


	//z has 15 significant
	*(src + 4) = *(src + 4) >> 1;

	tmp = (*(det + 2) | *(src + 5) << 8) & 0x8000;
	*(src + 5) = *(src + 5) & 0x7F;

	*(det + 2) = (*(det + 2) | *(src + 5) << 7) | tmp | (*(src + 4));
}
void analysisGYR(uint8_t* src, int16_t* det)
{
	*det = (*det | *(src + 1)) << 8 | *src;
	*(det + 1) = (*(det + 1) | *(src + 3)) << 8 | *(src + 2);
	*(det + 2) = (*(det + 2) | *(src + 5)) << 8 | *(src + 4);
}
void analysisACC(uint8_t* src, int16_t* det)
{
	*det = (*det | *(src + 1)) << 8 | *src;
	*(det + 1) = (*(det + 1) | *(src + 3)) << 8 | *(src + 2);
	*(det + 2) = (*(det + 2) | *(src + 5)) << 8 | *(src + 4);
}

void AnalysisIMU(unsigned char* src_data, CameronIMUSample* des_data)
{
	uint8_t mag_src[6], gyr_src[6], acc_src[6];
	int16_t mag_des[3] = { 0 };
	int16_t gyr_des[3] = { 0 };
	int16_t acc_des[3] = { 0 };


	memcpy(mag_src, (src_data ), 6);
	memcpy(gyr_src, (src_data + 6 + 2), 6);
	memcpy(acc_src, (src_data + 6 + 2 + 6), 6);

	analysisMAG(mag_src, mag_des);
	analysisGYR(gyr_src, gyr_des);
	analysisACC(acc_src, acc_des);

	des_data->magData[0] = ((float)mag_des[0] / 0x7fff) * IMU_Mag_RANGE_XY;
	des_data->magData[1] = ((float)mag_des[1] / 0x7fff) * IMU_Mag_RANGE_XY;
	des_data->magData[2] = ((float)mag_des[2] / 0x7fff) * IMU_Mag_RANGE_Z;

	des_data->gyroData[0] = ((float)gyr_des[0] / 0x7fff)*IMU_GYRO_RANGE;
	des_data->gyroData[1] = ((float)gyr_des[1] / 0x7fff)*IMU_GYRO_RANGE;
	des_data->gyroData[2] = ((float)gyr_des[2] / 0x7fff)*IMU_GYRO_RANGE;

	des_data->accelData[0] = ((float)acc_des[0] / 0x7fff)*IMU_ACCL_RANGE;
	des_data->accelData[1] = ((float)acc_des[1] / 0x7fff)*IMU_ACCL_RANGE;
	des_data->accelData[2] = ((float)acc_des[2] / 0x7fff)*IMU_ACCL_RANGE;
}

bool allocCameronInfo(CameronInfo *&info)
{
	if (info == NULL)
		info = new CameronInfo();
	if (info->device_name == NULL)
		info->device_name = new byte[MAX_STR+1];
	if (info->device_version == NULL)
		info->device_version = new byte[MAX_STR + 1];
	if (info->firmware_build == NULL)
		info->firmware_build = new byte[MAX_STR + 1];
	if (info->firmware_version == NULL)
		info->firmware_version = new byte[MAX_STR + 1];
	if (info->manufacturer_string == NULL)
		info->manufacturer_string = new byte[MAX_STR + 1];
	if (info->product_string == NULL)
		info->product_string = new byte[MAX_STR + 1];
	if (info->serial_number == NULL)
		info->serial_number = new byte[MAX_STR + 1];
	
	return true;
}
bool freeCameronInfo(CameronInfo *&info)
{
	if (info == NULL)
		return true;


	if (info->device_name != NULL)
		delete	info->device_name;
	info->device_name = NULL;

	if (info->device_version != NULL)
		delete	info->device_version;
		info->device_version = NULL;

	if (info->firmware_build != NULL)
		delete	info->firmware_build;
		info->firmware_build = NULL;

	if (info->firmware_version != NULL)
		delete	info->firmware_version;
		info->firmware_version = NULL;

	if (info->manufacturer_string != NULL)
		delete	info->manufacturer_string;
		info->manufacturer_string = NULL;	

	if (info->product_string != NULL)
		delete	info->product_string;
		info->product_string = NULL;

	if (info->serial_number != NULL)
		delete	info->serial_number;
		info->serial_number = NULL;

	delete info;
	info = NULL;
	return true;
}


//utility 
bool wchar2char(wchar_t* &pwchar, char* pchar)
{
	if (pwchar == NULL || pchar == NULL)
		return false;
	size_t len = wcslen(pwchar) + 1;
	wcstombs_s(0, pchar, len, pwchar, _TRUNCATE);
	return true;
}

bool CopyAndMove(unsigned char *&pDst, const unsigned char* pSrc, int lenth) {
	if (pDst == NULL || pSrc == NULL || lenth < 0)
	{
		ErrorHandler(TEXT("memory copy error"));
		return false;
	}
	memcpy(pDst, pSrc, lenth);
	pDst += lenth;
	return true;
}

>>>>>>> d8b605f20ef7180c384cb2be19a07c16f0a08d5f
